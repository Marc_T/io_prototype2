package com.example.marct.io_prototype2.localizerB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface LocalizerBTestDataDao {

    @Query("SELECT * FROM LocalizerBTestData")
    List<LocalizerBTestData> getAll();

    @Insert
    void insertAll(LocalizerBTestData... fingerprints);

    @Query("DELETE FROM LocalizerBTestData")
    void deleteAll();

}
