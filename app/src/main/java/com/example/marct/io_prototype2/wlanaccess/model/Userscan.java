package com.example.marct.io_prototype2.wlanaccess.model;

import com.example.marct.io_prototype2.Utility;

import java.util.ArrayList;

public class Userscan {



    private String direction;
    private ArrayList<AccessPoint> allAps;
    public static final String NORTH = "North";
    public static final String EAST = "East";
    public static final String SOUTH = "South";
    public static final String WEST = "West";

    public Userscan(String direction, ArrayList<AccessPoint> allAps) {
//                                                                    direction = Utility.removeSpecialCharacters(direction);
        this.direction = direction;
        this.allAps = allAps;
    }

    public String getDirection() {

//                                                                    direction = Utility.removeSpecialCharacters(direction);
        return direction;
    }

    public void setDirection(String direction) {

//                                                                direction = Utility.removeSpecialCharacters(direction);
        this.direction = direction;
    }

    public ArrayList<AccessPoint> getAllAps() {
        return allAps;
    }

    public void setAllAps(ArrayList<AccessPoint> allAps) {
        this.allAps = allAps;
    }

}
