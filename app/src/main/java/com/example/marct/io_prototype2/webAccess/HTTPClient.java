package com.example.marct.io_prototype2.webAccess;


import com.example.marct.io_prototype2.wlanaccess.model.Scan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import java.util.List;

public class HTTPClient {
    public static JSONObject dataToJSON(List<Scan> allScans) {
        JSONObject recordsToPost = new JSONObject();
        JSONObject fp = new JSONObject();
        JSONArray[] direction = new JSONArray[4];
        JSONObject directions = new JSONObject();
        try {
            fp.put("mapID", 1);
//            fp.put("mapID", allScans.get(0).getfPrint().getMapid());
            DecimalFormat df = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
            fp.put("x", Float.valueOf(df.format(allScans.get(0).getfPrint().getXcoord())));
            fp.put("y", Float.valueOf(df.format(allScans.get(0).getfPrint().getYcoord())));

            for (int dir = 0; dir < 4; dir++) {
                direction[dir] = new JSONArray();
                for (int i = 0; i < allScans.get(dir).getAllAps().size(); i++) {
                    JSONObject fpJson_rss = new JSONObject();
                    fpJson_rss.put("mac", allScans.get(dir).getAllAps().get(i)
                            .getMacAdress());
                    fpJson_rss.put("ssid", allScans.get(dir).getAllAps().get(i)
                            .getSsid());
                    fpJson_rss.put("strength", allScans.get(dir).getAllAps()
                            .get(i).getAverageSignal());
                    direction[dir].put(fpJson_rss);
                }
                switch (dir) {
                    case 0:
                        directions.put("Nord", direction[dir]);
                        break;
                    case 1:
                        directions.put("Ost", direction[dir]);
                        break;
                    case 2:
                        directions.put("Sued", direction[dir]);
                        break;
                    case 3:
                        directions.put("West", direction[dir]);
                        break;
                }
            }
            fp.put("directions", directions);
            recordsToPost=fp;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return recordsToPost;

    }


}
