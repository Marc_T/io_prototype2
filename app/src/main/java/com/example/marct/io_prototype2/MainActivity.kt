package com.example.marct.io_prototype2

import android.Manifest
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import com.example.marct.io_prototype2.database.AppDataBase
import com.example.marct.io_prototype2.database.DbWorkerThread
import com.example.marct.io_prototype2.database.Point
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager
import android.R.attr.level
import android.app.AlertDialog
import android.content.*
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Context.SENSOR_SERVICE
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.Drawable
import android.net.wifi.ScanResult
import android.os.Build
import com.example.marct.io_prototype2.compass.CompassListener
import com.example.marct.io_prototype2.localizerB.*
import com.example.marct.io_prototype2.wifiData.DatabaseHandler
import com.example.marct.io_prototype2.wifiData.Fingerprint
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_compass.view.*


class MainActivity : AppCompatActivity(), SensorEventListener {


    private lateinit var mSensorManager: SensorManager
    private lateinit var mWifiListener: WifiManager
    private val PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1001


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Do something with granted permission
            mWifiListener.scanResults
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mWifiListener = baseContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        var canvasView = findViewById<CanvasView>(R.id.canvasView)
        canvasView.setMainActivity(this)
        canvasView.setWifiManager(mWifiListener)
//
//        val wifiManager = baseContext.getSystemService(Context.WIFI_SERVICE) as WifiManager


        mSensorManager = baseContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)?.
                also { magneticField ->
                    mSensorManager.registerListener(
                            this,
                            magneticField,
                            SensorManager.SENSOR_DELAY_NORMAL,
                            SensorManager.SENSOR_DELAY_UI
                    )
                }

        btn_locate.setOnClickListener {
            canvasView.findMe()
//            canvasView.automaticFindMe()
//            btn_locate.isEnabled = false
        }

//        button_simplify.setOnClickListener {
//            canvasView.simplify()
//        }

        button_dostuff.setOnClickListener {
            canvasView.doStuff()

        }

        btn_copytestdata.setOnClickListener {
            canvasView.copyTestData();
        }


        btn_removefps.setOnClickListener {
            canvasView.deleteData();
        }

        btn_test.setOnClickListener {
            canvasView.runNextTest();
        }

//        btn_removefps.isEnabled = false


    }


    override fun onSensorChanged(event: SensorEvent?) {

    }


    fun setDegree(degree: Float) {
        tv_currentdirection.text = "" + degree;
    }

    fun getZoom(): Float {
        return zoomLayout.zoom;
    }

    var scanIteration = 1;
    fun setCurrentScanText(a: String) {
        Log.d("FT3", "TRIGGER A")
        tv_currentScan.text = "Iteration: " + scanIteration + " \n" + a;
        scanIteration++
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("FT4", "RESULT RECEIVED IN MAINACTIVITY")
        val i = 0
//        Log.d("FT2", data.)
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 2 && data != null) {
            Log.d("FT5", "CASE Z")



//            if(data!!.getStringExtra("result").equals("LOCALIZERB_ADDED_RESULT")) {
                canvasView.printLocalizerBFingerprints();
//            }


        }


    }


    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}



class CanvasView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var db: AppDataBase? = null
    private lateinit var mDbWorkerThread: DbWorkerThread

    var radius: Float = 20f
    var moved: Boolean = false
    private val mUiHandler = Handler()

    var currentPoint: FloatArray? = null
    var actualPosition: FloatArray? = null
    var location: FloatArray? = null


    var sm: SensorManager? = null;
    var cl: CompassListener? = null;
    var currentTestsetDataIndex = 0;
    var markerActual: Bitmap? = null;
    var markerFound: Bitmap? = null;

//    lateinit var localizerA: LocalizerA

    var localizerFHDMaps: LocalizerFHDMaps? = null
    var localizerFHDmapsJava: LocalizerFHDMapsJAVA? = null
    var wifimanager: WifiManager? = null
    var localizerBScanDatas: MutableList<LocalizerBScanData>? = null;
    lateinit var mainactivity: AppCompatActivity

    fun setMainActivity(mainActivity: AppCompatActivity) {
        mainactivity = mainActivity
        localizerFHDMaps = LocalizerFHDMaps(context, mainactivity)
    }

    fun runNextTest() {
        val localizerB = LocalizerB.getInstance();

        if(currentTestsetDataIndex >= localizerB.testsetData.size) {
            currentTestsetDataIndex = 0;
        }

        // testdatensatz holen
        val testdata = localizerB.testsetData.get(currentTestsetDataIndex)

//        doFindMe(testdata.scanResults);
//        val scanResult = ScanResult()

        doFindMeEigeneAlgos(testdata.scanResults!!);
        actualPosition = FloatArray(2)
        actualPosition!![0] = testdata.mapPosition!!.x;
        actualPosition!![1] = testdata.mapPosition!!.y;


        currentTestsetDataIndex++;
        mainactivity.btn_test.text = "Run Test " + currentTestsetDataIndex
    }

    fun setWifiManager(_wfman: WifiManager) {
        wifimanager = _wfman
        localizerFHDmapsJava = LocalizerFHDMapsJAVA(context, wifimanager, this)

                                                                        // NOTE: do here, should be in a constructor
                                                                        sm =  context.getSystemService(SENSOR_SERVICE) as SensorManager;
                                                                        cl = CompassListener(sm);
                                                                        cl!!.setMainactivity(mainactivity as MainActivity);

                                                                        markerActual= BitmapFactory.decodeResource(getResources(), R.drawable.marker_actual);
                                                                        markerFound= BitmapFactory.decodeResource(getResources(), R.drawable.marker_found);

//                                                                        mCustomImage= BitmapFactory.decodeResource(getResources(), R.drawable.marker2);
//                                                                        mCustomImage = context.getResources().getDrawable(R.drawable.marker);


    }

    fun deleteData() {


        var choices = arrayOf("DELETE FHDMaps Fingerprints", "DELETE LocalizerB Fingerprints", "DELETE LocalizerB Testvalues")
        var builder = AlertDialog.Builder(context)
        builder.setItems(choices) { _, which ->
            // Get the dialog selected item
            val selected = choices[which]


            val alertDialogBuilder = AlertDialog.Builder(context)

            // set title
            alertDialogBuilder.setTitle(selected + "?")

            // set dialog message
            alertDialogBuilder
                    .setMessage(selected + "?")
                    .setCancelable(false)
                    .setPositiveButton("Ja") {dialog, which ->

                        val db = AppDataBase.getInstance(context)
                        val mDbWorkerThread = DbWorkerThread("dbWorkerThread")
                        mDbWorkerThread.start()


                        if(selected == "DELETE FHDMaps Fingerprints") {


                            val dbHandle = DatabaseHandler(context)
                            dbHandle.resetDB()
                            Toast.makeText(context, "DELETED FROM FHDMAPS FINGERPRINT DB", Toast.LENGTH_SHORT).show()
                            doStuff()
                        }
                        else if(selected == "DELETE LocalizerB Fingerprints") {

                            val task = Runnable {
                                db!!.localizerBScanDataDao().deleteAll()
                                mUiHandler.post {
                                    Toast.makeText(context, "DELETED FROM LOCALIZERB FINGERPRINT DB", Toast.LENGTH_SHORT).show()
//                                    invalidate()
                                    doStuff()
                                }
                            }
                            mDbWorkerThread.postTask(task)


                        }
                        else if(selected == "DELETE LocalizerB Testvalues") {

                            val task = Runnable {
                                db!!.localizerBTestDataDao().deleteAll()
                                mUiHandler.post {
                                    Toast.makeText(context, "DELETED FROM LOCALIZERB TESTDATA DB", Toast.LENGTH_SHORT).show()
//                                    invalidate()
                                    doStuff()
                                }

                            }
                            mDbWorkerThread.postTask(task)
                        }



                    }
                    .setNegativeButton("Abbrechen"){dialog, which -> dialog.cancel()}

            // create alert dialog
            val alertDialog = alertDialogBuilder.create()

            // show it
            alertDialog.show()


        }

        builder.show();



//
//        val alertDialogBuilder = AlertDialog.Builder(this@MainActivity)
//
//        // set title
//        alertDialogBuilder.setTitle("Alle Fps löschen?")
//
//        // set dialog message
//        alertDialogBuilder
//                .setMessage("Alle Fps löschen?")
//                .setCancelable(false)
//                .setPositiveButton("Ja") {dialog, which ->
//                    canvasView.removeFps()
//                }
//                .setNegativeButton("Abbrechen"){dialog, which -> dialog.cancel()}
//
//        // create alert dialog
//        val alertDialog = alertDialogBuilder.create()
//
//        // show it
//        alertDialog.show()


    }

    fun removeFps() {
                // FHDMAPS
            //        val dbHandle = DatabaseHandler(context)
            //        val fps = dbHandle.resetDB()
            //        Log.d("FT2", "DONE; DB RESETTED")


        // localizerB FPs aus DB löschen
        val db = AppDataBase.getInstance(context)
        val mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        val task = Runnable {
            db!!.localizerBScanDataDao().deleteAll()
            mUiHandler.post {
                Toast.makeText(context, "DELETED FROM LOCALIZERB DB", Toast.LENGTH_SHORT).show()
                invalidate()
            }
        }
        mDbWorkerThread.postTask(task)
    }

    fun copyTestData() {

        var choices = arrayOf("Copy LocalizerB Fingerprints", "Copy LocalizerB Testvalue")
        var builder = AlertDialog.Builder(context)
        builder.setItems(choices) { _, which ->
            // Get the dialog selected item
            val selected = choices[which]

            if(selected == "Copy LocalizerB Fingerprints") {


                val db = AppDataBase.getInstance(context)
                val mDbWorkerThread = DbWorkerThread("dbWorkerThread")
                mDbWorkerThread.start()

                // fingerprints
                val task = Runnable {
                    val all = db !!.localizerBScanDataDao().all
                    mUiHandler.post {
                        val localizerB = LocalizerB.getInstance();
                        localizerB.scanDatas = all


                        val localizerBTempData = localizerB.scanDatas
                        val jsonString = localizerB.fpsToJsonString(1f);
                        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
                        val clip = ClipData.newPlainText("label", jsonString);
                        clipboard.setPrimaryClip(clip);

                        mUiHandler.post{
                            Log.d("FT7", jsonString)
                            Toast.makeText(context, "copied localizerB fingerprints to clipboard, " + localizerBTempData.size + " fingerprints inside", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                mDbWorkerThread.postTask(task)


            }
            else if(selected == "Copy LocalizerB Testvalue") {

                val db = AppDataBase.getInstance(context)
                val mDbWorkerThread = DbWorkerThread("dbWorkerThread")
                mDbWorkerThread.start()

                // fingerprints
                val task = Runnable {
                    val all = db !!.localizerBTestDataDao().all
                    mUiHandler.post {
                        val localizerB = LocalizerB.getInstance();
                        localizerB.testsetData = all


                        val localizerBTempData = localizerB.testsetData
                        val jsonString = localizerB.testsetDataToJsonString();
                        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
                        val clip = ClipData.newPlainText("label", jsonString);
                        clipboard.setPrimaryClip(clip);

                        mUiHandler.post{
                            Log.d("FT7", jsonString)
                            Toast.makeText(context, "copied localizerB  testvalues to clipboard, " + localizerBTempData.size + " values inside", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                mDbWorkerThread.postTask(task)


            }

        }

        builder.show();




//        val localizerBTempData = LocalizerB.getInstance().testsetData

//        val localizerB = LocalizerB.getInstance();
//        val jsonString = localizerB.testsetDataToJsonString();
//        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
//        val clip = ClipData.newPlainText("label", jsonString);
//        clipboard.setPrimaryClip(clip);
//        Log.d("FT6", "currently containing " + localizerBTempData.size + " testdatascans, copied to clipboard")
//        Toast.makeText(context, "copied testdata to clipboard, " + localizerBTempData.size + " fingerprints inside", Toast.LENGTH_SHORT).show();




    }

    fun printLocalizerBFingerprints() {



        val db = AppDataBase.getInstance(context)
        val mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        // fingerprints
        val task = Runnable {
            val all = db!!.localizerBScanDataDao().all
            mUiHandler.post {
                Log.d("FT5", "FOUND " + all.size + " FPS for localizerB");
                val localizerB = LocalizerB.getInstance();
                localizerB.scanDatas = all

//                Log.d("FT5", "" + localizerB.scanDatas.size)

//                val jsonString = localizerB.fpsToJsonString( (mainactivity as MainActivity).getZoom());
//
//                val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
//
//                val clip = ClipData.newPlainText("label", jsonString);
//                clipboard.setPrimaryClip(clip);
//                Toast.makeText(context, "COPIED LOCALIZERB FINGERPRINTS TO CLIPBOARD", Toast.LENGTH_SHORT).show()
//                Log.d("FT5", jsonString)
                localizerBScanDatas = localizerB.scanDatas
                invalidate()

            }
        }
        mDbWorkerThread.postTask(task)


        // testdata
        val taskTestdata = Runnable {
            val all = db!!.localizerBTestDataDao().all
            mUiHandler.post {
                val localizerB = LocalizerB.getInstance();
                localizerB.testsetData = all
//
//                val jsonString = localizerB.fpsToJsonString( (mainactivity as MainActivity).getZoom());
//
//                val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
//
//                val clip = ClipData.newPlainText("label", jsonString);
//                clipboard.setPrimaryClip(clip);
//                Toast.makeText(context, "COPIED LOCALIZERB FINGERPRINTS TO CLIPBOARD", Toast.LENGTH_SHORT).show()
//                Log.d("FT5", jsonString)
//                locali = localizerB.scanDatas
                invalidate()

            }
        }
        mDbWorkerThread.postTask(taskTestdata)







//        val localizerB = LocalizerB.getInstance();
//
//        localizerBScanDatas = localizerB.scanDatas
//
//        invalidate()
    }


    fun setCurrentScanText(a: String) {
        (mainactivity as MainActivity).setCurrentScanText(a)

    }

    fun doStuff() {

                                                                    val dbHandle = DatabaseHandler(context)
                                                                    val fps = dbHandle.getAllFp(0)
//                                                                    Log.d("FT2", "DONE")
//                                                                    Log.d("FT2", "" + fps.size)
//
//
//                                                                    var str = ""
//                                                                    for(fp in fps) {
//                                                            //            Log.d("FT2", "FOUND")
//                                                            //            Log.d("FT2", "" + fp.fpid)
//                                                            //            Log.d("FT2", "" + fp.mapid)
//                                                            //            Log.d("FT2", "" + fp.timestamp)
//                                                            //            Log.d("FT2", "" + fp.xcoord)
//                                                            //            Log.d("FT2", "" + fp.ycoord)
//                                                            //            Log.d("FT2", "FOUND")
//                                                            //            fp.
//
//                                                                        // TODO: alle eingescannten fingerprints in einen jsonstring packen, ausgeben und mal durchschauen
//
//                                                                        str += "{ fpId: ${fp.fpid}, "
//                                                                    }


                                                                    localizerFHDMaps!!.setFps(fps)
//                                                                    invalidate()



//                        val localizerB = LocalizerB.getInstance();
//
//
//                        val jsonString = localizerB.fpsToJsonString( (mainactivity as MainActivity).getZoom());
//                        Log.d("FT4", "JSONSTRING");
//                        Log.d("FT4", "" + jsonString);

                    //        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager;
                    //
                    //        val clip = ClipData.newPlainText("label", jsonString);
                    //        clipboard.setPrimaryClip(clip);


        // localizerB FPs aus DB holen und malen
        printLocalizerBFingerprints()





    }

    init{

//        localizerA = LocalizerA(context)

        val touchListener: View.OnTouchListener = OnTouchListener { v, event ->
            if (event.actionMasked == MotionEvent.ACTION_UP) {
                if(!moved) {
                    val lastTouchDownXY = FloatArray(2)
                    lastTouchDownXY[0] = event.x
                    lastTouchDownXY[1] = event.y
                    Log.d("FT1", "${lastTouchDownXY[0]} | ${lastTouchDownXY[1]}")
                    currentPoint = lastTouchDownXY


                                                                // add fingerprint
                                            //                    localizerA.makeFingerprint(lastTouchDownXY[0], lastTouchDownXY[1])
                                                                val mapPos = MapPosition(lastTouchDownXY[0], lastTouchDownXY[1])
                                            //                                                                       localizerFHDMaps!!.makeFingerprint(mapPos)

//                                                                val intent = Intent(_context, ScanActivity::class.java)
//                                                                intent.putExtra("Fingerprint", fPrint)


                                                                  // -- für localizerB fingerprint erstellen
//                                                                val intent = Intent(context, localizerBScanActivity::class.java)
//                                                                intent.putExtra("mapposition_x", mapPos.x)
//                                                                intent.putExtra("mapposition_y", mapPos.y)
//                                                                intent.putExtra("TESTSET", false)
//                                                                mainactivity.startActivityForResult(intent, 2)
//
//                // --- für localizerB testset
//                    val intent = Intent(context, localizerBScanActivity::class.java)
//                    intent.putExtra("mapposition_x", mapPos.x)
//                    intent.putExtra("mapposition_y", mapPos.y)
//                    intent.putExtra("TESTSET", true)
//                    mainactivity.startActivityForResult(intent, 2)


                    var choices = arrayOf("Create FHDMaps Fingerprint", "Create LocalizerB Fingerprint", "Create LocalizerB Testvalue", "Put actual Position", "Remove actual Position")
                    var builder = AlertDialog.Builder(context)
                    builder.setItems(choices){_, which ->
                        // Get the dialog selected item
                        val selected = choices[which]

                        if(selected == "Create FHDMaps Fingerprint") {
//                            Toast.makeText(context, "FHDMAPS FINGERPRINT", Toast.LENGTH_SHORT).show()
                            localizerFHDMaps!!.makeFingerprint(mapPos)
                        }
                        else if(selected == "Create LocalizerB Fingerprint") {
//                            Toast.makeText(context, "LOCALIZERB FINGERPRINT", Toast.LENGTH_SHORT).show()
                            val intent = Intent(context, localizerBScanActivity::class.java)
                            intent.putExtra("mapposition_x", mapPos.x)
                            intent.putExtra("mapposition_y", mapPos.y)
                            intent.putExtra("TESTSET", false)
                            mainactivity.startActivityForResult(intent, 2)
                        }
                        else if(selected == "Create LocalizerB Testvalue") {
//                            Toast.makeText(context, "LOCALIZERB TESTVALUE", Toast.LENGTH_SHORT).show()
                                val intent = Intent(context, localizerBScanActivity::class.java)
                                intent.putExtra("mapposition_x", mapPos.x)
                                intent.putExtra("mapposition_y", mapPos.y)
                                intent.putExtra("TESTSET", true)
                                mainactivity.startActivityForResult(intent, 2)
                        }
                        else if(selected == "Put actual Position") {
                            actualPosition = currentPoint
                            invalidate()

                        }
                        else if(selected == "Remove actual Position") {
                            actualPosition = null
                            invalidate()

                        }


                    }


//                    builder.setItems(colors, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // the user clicked on colors[which]
//                        }
//                    });
                    builder.show();


                    invalidate()
                }
            }
            else if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                moved = false
            }
            else if(event.actionMasked == MotionEvent.ACTION_SCROLL) {
                moved = true
            }
            // let the touch event pass on to whoever needs it
            true
        }
        this.setOnTouchListener(touchListener)


//        localizerA.initialize()
        invalidate()

    }


    var locations: List<FingerprintA>? = null
    var fhdmapsLocation: Array<Float>? = null
    var localizerBLocation: ArrayList<LocalizerBPositionResult>? = null;

    fun simplify() {
//        localizerA.simplify()
        invalidate()
    }

//    fun setCurrentScanText() {
//        mainactivity.
//    }

    var lastWifiListOutput = ""
    fun findMe() {
//        val fingerprints = localizerA.findMe()

//        Log.d("FT1", "" + (fingerprint == null))
//
//        if(fingerprints == null) {
//            Log.d("FT1", "COULD NOT BE LOCATED")
////            location = null
//            locations = mutableListOf<FloatArray>()
//        } else {
////            location = FloatArray(2)
////            location!![0] = fingerprint!!.canvasPosX
////            location!![1] = fingerprint!!.canvasPosY
////            Log.d("FT2", "1 " + fingerprint!!.canvasPosX + " " + fingerprint!!.canvasPosY)
//
//            val cLocation = FloatArray(2)
//
//        }

//        locations = localizerA.findMe()


        val newWifiList = wifimanager!!.scanResults


        var newoutput = ""
        for (sr in newWifiList) {
            newoutput += sr.BSSID + ": " + sr.level + "\n"
        }


        var hasChanged = true
        if (newoutput == lastWifiListOutput) {
            hasChanged = false
        }
        lastWifiListOutput = newoutput

        var message = "Current scan IS NOT THE SAME as last"
        if(!hasChanged) {
            message = "Current scan IS THE SAME as last"
        }
        setCurrentScanText(message)


//        // -------
//                    // FHDmaps localization
                    val fp = localizerFHDMaps!!.findMe(localizerFHDmapsJava!!, newWifiList, cl!!.orientation)
                    fhdmapsLocation = fp
                    // -------
//
//                    // localizerB localization
//                    val localizerB = LocalizerB.getInstance()
//                    localizerBLocation = localizerB.findMe(newWifiList, cl!!.degree)
//
//                    invalidate()



        val customscanresults = ArrayList<CustomScanResult>();
        for(item in newWifiList) {
            val csr = CustomScanResult(item.BSSID, item.SSID, item.level, item.frequency);
            customscanresults.add(csr);
        }


        doFindMeEigeneAlgos(customscanresults)

    }


    fun doFindMeEigeneAlgos(newWifiList: List<CustomScanResult>) {

        // localizerB localization
        val localizerB = LocalizerB.getInstance()
        localizerBLocation = localizerB.findMe(newWifiList, cl!!.degree)

        invalidate()

    }

//
//    fun automaticFindMe() {
//        localizerFHDMaps!!.automaticFindMe(localizerFHDmapsJava!!, canvasView, cl!!.orientation)
//
//    }

//    fun setLocalizedFHDMapsFingerprint(fp: Fingerprint) {
    fun setLocalizedFHDMapsFingerprint(xAndY: Array<Float>?) {


        fhdmapsLocation = xAndY
    setCurrentScanText("")
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // custom drawing code here
        val paint = Paint()
        paint.setStyle(Paint.Style.FILL)

        // Draw floorplan
//        val d = resources.getDrawable(R.drawable.floorplan1)
        val d = resources.getDrawable(R.drawable.hsdgebaeuderesized)
        d.setBounds(left, top, right, bottom)
        d.draw(canvas)

        // get all scanned fingerprints and draw them


        // draw blue circle with anti aliasing turned on
        paint.setAntiAlias(true)
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f;

        // localizerB fps
        if(localizerBScanDatas != null) {
            for(scandata in localizerBScanDatas!!) {
                paint.setColor(Color.GREEN)
//                Log.d("FT2", "PAINTING FP @ " + fingerprint.xcoord + " | " + fingerprint.ycoord)
                canvas.drawCircle(scandata.mapPosition!!.x, scandata.mapPosition!!.y, radius, paint)
            }
        }


        // fhdmaps fps
        if(localizerFHDMaps != null && localizerFHDMaps!!.fingerprints != null) {
            for (fingerprint in localizerFHDMaps!!.fingerprints!!) {
                paint.setColor(Color.RED)
                Log.d("FT2", "PAINTING FP @ " + fingerprint.xcoord + " | " + fingerprint.ycoord)
                canvas.drawCircle(fingerprint.xcoord, fingerprint.ycoord, radius, paint)
            }
        }



        if(localizerBLocation != null) {
            for(cLocation in localizerBLocation!!) {

                paint.style = Paint.Style.FILL
                paint.setColor(cLocation.drawColor)
                canvas.drawCircle(cLocation.mapPosition.x, cLocation.mapPosition.y, (cLocation.drawRadius).toFloat() * 2, paint)

                paint.style = Paint.Style.STROKE
                canvas.drawCircle(cLocation.mapPosition.x, cLocation.mapPosition.y, (cLocation.errorRadius).toFloat(), paint)

                if(cLocation.algoName.equals("B1")) {

                    canvas.drawBitmap(markerFound, cLocation.mapPosition.x- 180,  cLocation.mapPosition.y - 180, paint);
                }

            }
        }


        // localizerB temporary testscans
        paint.style = Paint.Style.FILL
        val localizerBTempData = LocalizerB.getInstance().testsetData
        Log.d("FT6", "currently containing " + localizerBTempData.size + " testdatascans")
        for(scandata in localizerBTempData) {
            paint.setColor(Color.MAGENTA)
            paint.style = Paint.Style.STROKE
            canvas.drawCircle(scandata.mapPosition!!.x, scandata.mapPosition!!.y, radius, paint)

        }


        //
        // draw fhdmaps location
        if(fhdmapsLocation != null) {
            paint.style = Paint.Style.FILL
            paint.setColor(Color.RED)
            canvas.drawCircle(fhdmapsLocation!![0], fhdmapsLocation!![1], 30f, paint)

        }

        if(actualPosition != null) {
            paint.setColor(Color.RED)
//            canvas.drawRect(actualPosition!![0] - 10, actualPosition!![1] - 10, actualPosition!![0] + 10, actualPosition!![1] + 10, paint)
        //            val imageBounds = canvas.getClipBounds();  // Adjust this for where you want it
        //                mCustomImage!!.setBounds(imageBounds);
        //                mCustomImage!!.draw(canvas);

//                    val b= BitmapFactory.decodeResource(getResources(), R.drawable.marker_small);
                    canvas.drawBitmap(markerActual, actualPosition!![0]- 180, actualPosition!![1] - 180, paint);


        }

    }

}

