package com.example.marct.io_prototype2.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface PointDao {

    @Query("SELECT * FROM point")
    List<Point> getAll();

    @Insert
    void insertAll(Point... points);

    @Query("DELETE FROM point")
    void deleteAll();

}
