package com.example.marct.io_prototype2.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.example.marct.io_prototype2.localizerB.LocalizerBScanData
import com.example.marct.io_prototype2.localizerB.LocalizerBScanDataDao
import com.example.marct.io_prototype2.localizerB.LocalizerBTestData
import com.example.marct.io_prototype2.localizerB.LocalizerBTestDataDao


@Database(entities = [LocalizerBScanData::class, LocalizerBTestData::class], version = 3)
//@Database(entities = [Point::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDataBase : RoomDatabase() {

//    abstract fun pointDao(): PointDao
    abstract fun localizerBScanDataDao(): LocalizerBScanDataDao
    abstract fun localizerBTestDataDao(): LocalizerBTestDataDao

    companion object {
        private var INSTANCE: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase? {
            if (INSTANCE == null) {
                synchronized(AppDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDataBase::class.java, "IO_Prototype2DB")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}