package com.example.marct.io_prototype2.localizerB;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.support.v4.widget.TextViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.marct.io_prototype2.MainActivity;
import com.example.marct.io_prototype2.MapPosition;
import com.example.marct.io_prototype2.R;
import com.example.marct.io_prototype2.compass.CompassListener;
import com.example.marct.io_prototype2.database.AppDataBase;
import com.example.marct.io_prototype2.database.DbWorkerThread;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class localizerBScanActivity extends AppCompatActivity {


    // device sensor manager
    private SensorManager mSensorManager;
    float degree;
    MapPosition mapPos;
    List<ScanResult> wifiList;
    SensorManager sensorManager;
    CompassListener compassListener;
    WifiChangeListener wifiChangeListener;
    String previousoutput = "";
    MainActivity mainActivity;
    boolean isTestSetCreation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localizer_bscan);
        Bundle b = getIntent().getExtras();


        mapPos = new MapPosition(b.getFloat("mapposition_x"), b.getFloat("mapposition_y"));
        isTestSetCreation = b.getBoolean("TESTSET");

        Log.d("FT6", "TESTCREATION?" + isTestSetCreation);


        Log.d("FT4", "" + mapPos.toString());

        sensorManager = (SensorManager) getBaseContext().getSystemService(SENSOR_SERVICE);
        compassListener = new CompassListener(sensorManager);
        compassListener.setActivity(this);

        wifiChangeListener = new WifiChangeListener(getBaseContext(), this);
        setWifiList(wifiChangeListener.getNewList());

        ((Button) findViewById(R.id.btn_updatelist)).setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    setWifiList(wifiChangeListener.getNewList());
                }
            }
        );

        ((Button) findViewById(R.id.btn_showssids)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        String output = getOutputWIthSSIDS();
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(localizerBScanActivity.this);
                        builder.setTitle("CURRENT APs:")
                                .setMessage(output)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        dialog.dismiss();
                                    }
                                })
//                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do nothing
//                                    }
//                                })
                                .show();

                    }
                }
        );

        ((Button) findViewById(R.id.btn_scan)).setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if(wifiList != null) {

                        // wifilist nach customscan umbauen
                        ArrayList<CustomScanResult> customScanResults = new ArrayList<>();
                        for(ScanResult sr: wifiList) {
                            CustomScanResult csr  = new CustomScanResult(sr.BSSID, sr.SSID, sr.level, sr.frequency);
                            customScanResults.add(csr);
                        }


                        if(!isTestSetCreation) { // fingerprint  => in db rein
                            final LocalizerBScanData localizerBScanData = new LocalizerBScanData(null, degree, mapPos, customScanResults);  // TODO ??
                            // in db rein
                            final AppDataBase db = AppDataBase.Companion.getInstance(getBaseContext());
                            DbWorkerThread mDbWorkerThread = new DbWorkerThread("dbWorkerThread");
                            mDbWorkerThread.start();

                            Runnable task = new Runnable() {
                                public void run() {
                                    db.localizerBScanDataDao().insertAll(localizerBScanData);
                                }
//
//                                                mUiHandler.post {
//                                                    // if task was completed, subtract APs from counter
//                                                    if (selectedTask.completed) {
//                                                        AchievementPoints.points -= selectedTask.achievementPoints
//                                                    }
//
//                                                    // Update List
//                                                    refreshList()
//                                                    Toast.makeText(context, "TASK DELETED", Toast.LENGTH_SHORT).show()
//                                                }
                            };
                            mDbWorkerThread.postTask(task);
                            Toast.makeText(getBaseContext(), "ADDING FINGERPRINT TO DB", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            // sonst => in temporäre liste rein
//                            LocalizerB.getInstance().testsetData.add(localizerBScanData);
//                            Toast.makeText(getBaseContext(), "ADDED FINGERPRINT TO TEMP TESTSET", Toast.LENGTH_SHORT).show();

                            final LocalizerBTestData localizerBTestData = new LocalizerBTestData(null, degree, mapPos, customScanResults);  // TODO ??
                                // in db rein
                                final AppDataBase db = AppDataBase.Companion.getInstance(getBaseContext());
                                DbWorkerThread mDbWorkerThread = new DbWorkerThread("dbWorkerThread");
                                mDbWorkerThread.start();

                                Runnable task = new Runnable() {
                                    public void run() {
                                        db.localizerBTestDataDao().insertAll(localizerBTestData);
                                    }
    //
    //                                                mUiHandler.post {
    //                                                    // if task was completed, subtract APs from counter
    //                                                    if (selectedTask.completed) {
    //                                                        AchievementPoints.points -= selectedTask.achievementPoints
    //                                                    }
    //
    //                                                    // Update List
    //                                                    refreshList()
    //                                                    Toast.makeText(context, "TASK DELETED", Toast.LENGTH_SHORT).show()
    //                                                }
                                };
                                mDbWorkerThread.postTask(task);
                                Toast.makeText(getBaseContext(), "ADDING TESTDATA TO DB", Toast.LENGTH_SHORT).show();

                        }

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "LOCALIZERB_ADDED_RESULT");
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    }
                }
            }
        );


        if(isTestSetCreation) {
            ((TextView) findViewById(R.id.tv_scantype)).setText("SCANNING FOR TESTSET");
        } else {
            ((TextView) findViewById(R.id.tv_scantype)).setText("SCANNING FOR FINGERPRINT (LOCALIZER B)");
        }

    }

    public String getOutputWIthSSIDS() {


        String outputWithSSIDS = "";
        for(ScanResult sr : wifiList) {
            outputWithSSIDS += sr.SSID + " >>> " + sr.BSSID + ": " + sr.level + "\n\n";
        }

        return outputWithSSIDS;

    }

    public void setWifiList(List<ScanResult> _wifiList) {

        wifiList = _wifiList;

        String output = "";
        for(ScanResult sr : wifiList) {
            output += sr.BSSID + ": " + sr.level + "\n";
        }


        boolean hasChanged = true;
        if(output.equals(previousoutput)) {
            hasChanged = false;
        }
        previousoutput = output;


        output += "\n\nHasChanged: " + hasChanged;
        ((TextView) findViewById(R.id.tv_wifidata)).setText(output);

    }

    public void setDegree(Float _degree) {
        degree = _degree;


        ((TextView) findViewById(R.id.tv_orientation)).setText("Orientation: " + degree);
    }

}

