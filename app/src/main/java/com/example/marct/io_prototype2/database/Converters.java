package com.example.marct.io_prototype2.database;

import android.arch.persistence.room.TypeConverter;
import android.net.wifi.ScanResult;

import com.example.marct.io_prototype2.MapPosition;
import com.example.marct.io_prototype2.localizerB.CustomScanResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Converters {
    @TypeConverter
    public static ArrayList<CustomScanResult> fromString(String value) {
//        Type listType = new TypeToken<List<ScanResult>>() {}.getType();
//        return new Gson().fromJson(value, listType);

        Type listType = new TypeToken<ArrayList<CustomScanResult>>() {}.getType();
        Gson gson = new Gson();

        return gson.fromJson(value, listType);
    }

    @TypeConverter
    public static String fromList(ArrayList<CustomScanResult> list) {
//        Gson gson = new Gson();
//        String json = gson.toJson(list);
//        return json;
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    @TypeConverter
    public static String fromMapPosition(MapPosition mapPosition) {
        Gson gson = new Gson();
        String json = gson.toJson(mapPosition);
        return json;
    }

    @TypeConverter
    public static MapPosition toMapPosition(String value) {
        return new Gson().fromJson(value, MapPosition.class);
    }


    @TypeConverter
    public static String fromScanResult(ScanResult scanResult) {
        Gson gson = new Gson();
        String json = gson.toJson(scanResult);
        return json;
    }

    @TypeConverter
    public static ScanResult toScanResult(String value) {
        return new Gson().fromJson(value, ScanResult.class);
    }

}