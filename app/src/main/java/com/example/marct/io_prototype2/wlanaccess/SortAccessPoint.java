package com.example.marct.io_prototype2.wlanaccess;


import java.util.Comparator;

import com.example.marct.io_prototype2.wlanaccess.model.AccessPoint;

public class SortAccessPoint implements Comparator<AccessPoint> {

    @Override
    public int compare(AccessPoint lhs, AccessPoint rhs) {
        // TODO Auto-generated method stub
        return lhs.getAverageSignal()-rhs.getAverageSignal();
    }

}
