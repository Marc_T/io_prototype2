package com.example.marct.io_prototype2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//public class ScanActivity extends AppCompatActivity {


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

//import webAccess.HTTPClient;
//import com.example.marct.io_prototype2.wifiData.DatabaseHandler;
import com.example.marct.io_prototype2.webAccess.HTTPClient;
import com.example.marct.io_prototype2.wifiData.DatabaseHandler;
import com.example.marct.io_prototype2.wifiData.Fingerprint;
import com.example.marct.io_prototype2.wlanaccess.SortAccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.AccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.Scan;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
//import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

//import com.app.wifireader.R;

/**
 * In dieser Klasse wird der gesamte Scanvorgang durchgef�hrt.
 *
 */
public class ScanActivity extends AppCompatActivity {

    private WifiManager wifiManager;
    private ArrayList<Scan> allScans = new ArrayList<Scan>();
    private Scan scan;

    private String currentDirection;

    private ProgressBar scanProgress;
    private Handler updateTextViewHandler = new Handler();

    private TextView direction;
    private ImageView compass;
    private Button btnScan = null;

    private Handler progressHandler;
    private ArrayList<AccessPoint> orderedAps;
    WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    HashMap<String, AccessPoint> allAps = new HashMap<String, AccessPoint>();
    private int scanningProgress = 0;
    Fingerprint fPrint = null;

//    private DatabaseHandler dbHandler = null;

    private static final int UPDATE_MODE = 1;
    private static final int CREATE_MODE = 2;
    private int currentMode = -1;

//    HTTPClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_scan);

        direction = (TextView) findViewById(R.id.direction);

        scanProgress = (ProgressBar) findViewById(R.id.scanningProgress);
        btnScan = (Button) findViewById(R.id.btnScan);
        currentDirection = Scan.NORTH;
        compass = (ImageView) findViewById(R.id.imageViewCompass);
        progressHandler = new Handler();
        receiverWifi = new WifiReceiver();
//
//        client = new HTTPClient();
//
        allScans.clear();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String loginresponse = client.fhdlogin();
//            }
//        }).start();

    }

    protected void onPause() {
        unregisterReceiver(receiverWifi);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    public void returnToMap(View view) {

        this.finish();
    }

    private void doComplete() {
        Log.d("post Fingerprint", "calling asyncTask..");
        HttpAsyncTask async = new HttpAsyncTask(allScans);
        async.execute("url");
        this.finish();
    }

    public void onClick(View view) {

        // Scanner initialisieren
//        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        // �berpr�fen ob das Wlan angeschaltet ist
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
            wifiManager.setWifiEnabled(true);
            Toast.makeText(this, "Wlan wurde angeschaltet", Toast.LENGTH_SHORT)
                    .show();
        }

        btnScan.setEnabled(false);

//        dbHandler = new DatabaseHandler(this);

        Thread t = new Thread() {

            @Override
            public void run() {


                Integer fpid = null;

                /**
                 * Hier wird �berpr�ft ob ein Fingerprint oder eine Fingerprint
                 * ID �bergeben wurde. Wenn ein Fingerprint �bergeben wurde,
                 * soll ein neuer Messpunkt in der Datenbank eingetragen werden
                 * und bei einer ID soll ein alter Messpunkt aktualisiert
                 * werden.
                 */
                Log.d("FT2", "Decision");
                if (getIntent().getSerializableExtra("Fingerprint") != null) {
                    Log.d("FT2", "CREATEMODE");

                    fPrint = (Fingerprint) getIntent().getSerializableExtra(
                            "Fingerprint");
                    currentMode = CREATE_MODE;

                } else if (getIntent().getSerializableExtra("fpid") != null) {
                    Log.d("FT2", "UPDATEMODE");

                    fpid = (Integer) getIntent().getSerializableExtra("fpid");
                    currentMode = UPDATE_MODE;
                }

                System.out.println(currentMode);

                doScan();


            }

        };
        t.start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void deleteMeasurePoint(int fpId) {
        Log.d("delete Fingerprint", "FP:" + fpId + ", calling asyncTask..");
        HttpAsyncTask async = new HttpAsyncTask(fpId);
        async.execute("url");
    }

    /**
     * Startet einen Scan
     */
    public void doScan() {
        scanningProgress = 0;
        allAps.clear();
        this.wifiManager.startScan();
    }

    /**
     * Ordnet alle Accesspoints absteigend und gibt sie in einer ArrayList
     * zur�ck
     *
     * @param allAps
     * @return orderedAps
     */
    private ArrayList<AccessPoint> orderAps(HashMap<String, AccessPoint> allAps) {
        ArrayList<AccessPoint> orderedAps = new ArrayList<AccessPoint>();

        orderedAps.addAll(allAps.values());

        Collections.sort(orderedAps,
                Collections.reverseOrder(new SortAccessPoint()));

        return orderedAps;
    }

    /**
     * Wird aufgerufen, wenn ein Scan in eine Himmelsrichtung abgeschlossen ist
     */
    private void scanComplete(){
        ArrayList<AccessPoint> aps = (ArrayList<AccessPoint>)orderedAps.clone();

        Log.d("FT2", "ADDING FINGERPRINT:");
        Log.d("FT2", "" + (fPrint == null));

        Scan scan = new Scan(currentDirection,aps,fPrint);
        allScans.add(scan);
        orderedAps.clear();
        updateTextViewHandler.post(new Runnable() {

            @Override
            public void run() {
                // showScan(scan);

                // Jedes mal die n�chste Himmelsrichtung ausw�hlen
                if (currentDirection.equals(Scan.NORTH)) {
                    currentDirection = Scan.EAST;
                    direction
                            .setText("Kompass nach Osten ausrichten und Scannen");
                    compass.setImageResource(R.drawable.compassosten);
                } else if (currentDirection.equals(Scan.EAST)) {
                    currentDirection = Scan.SOUTH;
                    direction
                            .setText("Kompass nach S�den ausrichten und Scannen");
                    compass.setImageResource(R.drawable.compasssueden);
                } else if (currentDirection.equals(Scan.SOUTH)) {
                    currentDirection = Scan.WEST;
                    direction
                            .setText("Kompass nach Westen ausrichten und Scannen");
                    compass.setImageResource(R.drawable.compasswesten);
                } else if (currentDirection.equals(Scan.WEST)) {

                    btnScan.setText("Abschlie�en");
                    btnScan.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            //Messpunkt abspeichern und die activity beenden
                            doComplete();
                        }
                    });

                }
                btnScan.setEnabled(true);
            }
        });
    }

    public class HttpAsyncTask extends AsyncTask<String, Void, String> {

        List<Scan> allScans;
        int id;

        public HttpAsyncTask(List<Scan> allScans) {
            this.allScans = allScans;
        }

        public HttpAsyncTask(int id) {
            this.id = id;
        }

        @Override
        protected String doInBackground(String... url) {
            Log.d("asyncTask", "do in background");
            if (allScans != null) {
                Log.d("asyncTask", "post FP");

//                String result = allScans.toString();
//                Log.d("FT2", "RESULT");
//                Log.d("FT2", result);

//                String output = "";
//                output = "Scans: " + allScans.size() + " ";
//                for(Scan scan: allScans) {
//                    output += " X: " + scan.getfPrint().getXcoord() + " Y: " + scan.getfPrint().getYcoord();
//                }
//                Log.d("FT2", "OUTPUT");
//                Log.d("FT2", output);

                // NOTE: allscans now has final result
                JSONObject res = HTTPClient.dataToJSON(allScans);
//                Log.d("FT2", "JSON:");
//                Log.d("FT2", res.toString());


                                        // ---
                                        // add to db
                                        DatabaseHandler dbHandler = new DatabaseHandler(getBaseContext());

                                        for(Scan scan: allScans) {
//                                            scan.setfPrint(fPrint);                             // TODO: ??????????? correct?

                                            Fingerprint fp = scan.getfPrint();
                                            if(fp != null) {
                                                Log.d("FT2", "LINKED FP: ");
                                                Log.d("FT2", "" + fp.getFpid());
                                                Log.d("FT2", "" + fp.getMapid());
                                                Log.d("FT2", "" + fp.getTimestamp());
                                                Log.d("FT2", "" + fp.getXcoord());
                                                Log.d("FT2", "" + fp.getYcoord());
                                            }
                                            else {

                                                Log.d("FT2", "NO FP LINKED");
                                            }

                                                                                // TODO: FUNKTIONIERT DAS?
                                                                                for(AccessPoint ap: scan.getAllAps()) {
                                                                                    String orig = ap.getSsid();
                                                                                    String newSSid = Utility.removeSpecialCharacters(orig);
                                                                                    ap.setSsid(newSSid);
                                                                                }




                                            dbHandler.addFP(scan);
                                        }

                                        // ---

                return res.toString();

//                return "response" + ";" + client.postFingerprint(allScans);
            }
//            else if (id != 0) {
//                if (client == null)
//                    Log.d("asyncTask", "client ist null");
//                Log.d("asyncTask", "delete FP:" + id);
//                String loginresponse = client.fhdlogin();
//                Log.d("loginresponse", loginresponse);
//                return "response" + ";" + client.deleteFingerprint(id);
//            } else {
//                Log.d("asyncTask", "do in Background error");
//                return "error";
//            }

            return "error";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("asyncTask", result);
            Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
        }

    }

    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            Log.d("FT2", "ONRECEIVE");
            Log.d("FT2", "ONRECEIVE ITERATION");
            Log.d("FT2", "" + scanningProgress);
            //Wenn der letzte Scan abgeschlossen ist
            if(wifiManager == null) {
                Log.d("FT2", "NPE triggered");
                wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            }

            wifiList = wifiManager.getScanResults();
            //Log.v("ScanAct", "wifiList"+wifiList);
            // Alle Wlans durchlaufen und die Signalst�rken ermitteln
            for (ScanResult sr : wifiList) {
                Log.v("ScanAct", "Scan: "+sr.BSSID+", "+sr.level);
                if (!allAps.containsKey(sr.BSSID)) {
                    allAps.put(sr.BSSID, new AccessPoint(sr.level, sr.SSID,
                            sr.BSSID, sr.frequency));
                } else {
                    allAps.get(sr.BSSID).addSignal(sr.level);
                }
            }
            Log.v("ScanAct", "next scan");
            scanningProgress++;
            progressHandler.post(new Runnable() {

                @Override
                public void run() {
                    scanProgress.setProgress(scanningProgress);
                }

            });
            if (scanningProgress < 3) {
                ScanActivity.this.wifiManager.startScan();
            } else {
                orderedAps = orderAps(allAps);
                scanComplete();
            }

        }
    }


}