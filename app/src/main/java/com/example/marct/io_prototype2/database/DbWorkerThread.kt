package com.example.marct.io_prototype2.database


import android.os.Handler
import android.os.HandlerThread

internal class DbWorkerThread(threadName: String) : HandlerThread(threadName) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler = Handler(looper)
        mWorkerHandler.post(task)
    }

}