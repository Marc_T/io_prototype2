package com.example.marct.io_prototype2.wifiData;


import java.io.Serializable;
import java.sql.Date;

public class Fingerprint implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8876380299210929611L;


    float xcoord;
    float ycoord;
    int mapid;
    int fpid;
    Date timestamp;

    public Fingerprint() {

    }

    public float getXcoord() {
        return xcoord;
    }

    public void setXcoord(float xcoord) {
        this.xcoord = xcoord;
    }

    public float getYcoord() {
        return ycoord;
    }

    public void setYcoord(float ycoord) {
        this.ycoord = ycoord;
    }

    public int getMapid() {
        return mapid;
    }

    public int getFpid() {
        return fpid;
    }

    public void setFpid(int fpid) {
        this.fpid = fpid;
    }

    public void setMapid(int mapid) {
        this.mapid = mapid;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}