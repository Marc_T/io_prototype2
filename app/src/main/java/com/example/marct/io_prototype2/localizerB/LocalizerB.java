package com.example.marct.io_prototype2.localizerB;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.ScanResult;
import android.graphics.Color;

import com.example.marct.io_prototype2.MapPosition;
import com.example.marct.io_prototype2.wlanaccess.model.Scan;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import kotlin.jvm.internal.MagicApiIntrinsics;

// https://www.snet.tu-berlin.de/fileadmin/fg220/courses/WS1112/snet-project/wifi-positioning_henniges.pdf
public class LocalizerB {

    // Eine (versteckte) Klassenvariable vom Typ der eigenen Klasse
    private static LocalizerB instance;

    private LocalizerB() {
    }

    public static LocalizerB getInstance() {
        if (LocalizerB.instance == null) {
            LocalizerB.instance = new LocalizerB();
        }
        return LocalizerB.instance;
    }


    List<LocalizerBScanData> localizerBScanDatas = new ArrayList<LocalizerBScanData>();
    public List<LocalizerBTestData> testsetData = new ArrayList<LocalizerBTestData>();

    public List<LocalizerBScanData> getScanDatas() {
        return localizerBScanDatas;
    }

    public void setScanDatas(List<LocalizerBScanData> datas) {
        localizerBScanDatas = datas;
    }

    public void addScanData(LocalizerBScanData scanData) {
        localizerBScanDatas.add(scanData);
    }

    public String testsetDataToJsonString() { // eigentlich die gleiche methode wie  fpsToJsonString



        String res = " { \"zoomfactor\": \"" + "NONE, THIS IS TESTDATA" + "\", ";
        int index = 0;

        for(LocalizerBTestData BscanData: testsetData) {

            res += "\"" + index + "\": ";
            res += "{ \"mapposition_x\": " + BscanData.mapPosition.getX() + ", ";
            res +=  "\"mapposition_y\": "  + BscanData.mapPosition.getY() + ", ";
            res += "\"direction\": " + BscanData.direction + ", ";
            res += "\"wifidata\": {";


            int innerIndex = 0;
//            for(ScanResult scanData: BscanData.scanResultsToList()) {
            for(CustomScanResult scanData: BscanData.scanResults) {
                res += "\"" + innerIndex + "\": { \"SSID\": \"" + scanData.getSSID() + "\", ";
                res += " \"BSSID\": \"" + scanData.getBSSID() + "\", ";
                res += " \"level\": \"" + scanData.getLevel() + "\", ";
                res += " \"frequency\": \"" + scanData.getFrequency() + "\"";

                if(innerIndex < (BscanData.scanResults.size() - 1)) {
                    res += "},";
                }
                else {
                    res += "}";
                }

                innerIndex++;
            }

            res += " } ";

            if(index < (localizerBScanDatas.size() - 1)) {
                res += "}, ";
            }
            else {
                res += "} ";
            }


            index++;

        }

        res += "}";
        return res;

    }

    public String fpsToJsonString(Float zoomfactor) {

        String res = " { \"zoomfactor\": \"" + zoomfactor + "\", ";
        int index = 0;

        for(LocalizerBScanData BscanData: localizerBScanDatas) {

            res += "\"" + index + "\": ";
            res += "{ \"mapposition_x\": " + BscanData.mapPosition.getX() + ", ";
            res +=  "\"mapposition_y\": "  + BscanData.mapPosition.getY() + ", ";
            res += "\"direction\": " + BscanData.direction + ", ";
            res += "\"wifidata\": {";


            int innerIndex = 0;
//            for(ScanResult scanData: BscanData.scanResultsToList()) {
            for(CustomScanResult scanData: BscanData.scanResults) {
                res += "\"" + innerIndex + "\": { \"SSID\": \"" + scanData.getSSID() + "\", ";
                res += " \"BSSID\": \"" + scanData.getBSSID() + "\", ";
                res += " \"level\": \"" + scanData.getLevel() + "\", ";
                res += " \"frequency\": \"" + scanData.getFrequency() + "\"";

                if(innerIndex < (BscanData.scanResults.size() - 1)) {
                    res += "},";
                }
                else {
                    res += "}";
                }

                innerIndex++;
            }

            res += " } ";

            if(index < (localizerBScanDatas.size() - 1)) {
                res += "}, ";
            }
            else {
                res += "} ";
            }


            index++;

        }

        res += "}";
        return res;

    }






    class BssidAndSSid {
        public String BSSID;
        public String SSID;
        public BssidAndSSid(String _BSSID, String _SSID) {
            BSSID = _BSSID;
            SSID = _SSID;
        }
    }

//    public ArrayList<LocalizerBPositionResult> findMe(List<ScanResult> currentScan, Float degree) {
    public ArrayList<LocalizerBPositionResult> findMe(List<CustomScanResult> currentScan, Float degree) {

        // alle verschiedenen BSSIDs in den fingerprints sammeln
        List<BssidAndSSid> allBSSIDsAndSSids = new ArrayList<BssidAndSSid>();
        List<LocalizerBScanData> cfingerprints = localizerBScanDatas;
        for(LocalizerBScanData fingerprint : cfingerprints) {

            for(CustomScanResult scanData: fingerprint.scanResults) {

                String BSSID = scanData.getBSSID();
                String SSID = scanData.getSSID();
                boolean exists = false;
                for(BssidAndSSid existingBSSidAndSSid: allBSSIDsAndSSids) {

                    String existingBSSID = existingBSSidAndSSid.BSSID;

                    if(existingBSSID.equals(BSSID)) {
                        exists = true;
                        break;
                    }
                }

                if(!exists) {
//                    allBSSIDs.add(BSSID);
                    allBSSIDsAndSSids.add(new BssidAndSSid(BSSID, SSID));
                }
            }
        }

        // für jeden fingerprint einen neuen fingerprint generieren, der für jede BSSID einen wert hat
        List<LocalizerBScanData> fingerprints = new ArrayList<>();
        for(LocalizerBScanData cfingerprint : cfingerprints) {
            LocalizerBScanData newFingerprint = new LocalizerBScanData(cfingerprint.getUid(), cfingerprint.direction, cfingerprint.mapPosition, cfingerprint.scanResults);

            // alle existierenden BSSIDs durchgehen, jede die noch nicht drinn ist rein mit -100
            for(BssidAndSSid existingBSSidAndSSid : allBSSIDsAndSSids) {

                boolean exists = false;
                String existingBSSID = existingBSSidAndSSid.BSSID;
                String existingSSID = existingBSSidAndSSid.BSSID;

                for(CustomScanResult scanData: newFingerprint.scanResults) {
                    String cBssid = scanData.getBSSID();

                    if(existingBSSID.equals(cBssid)) {
                        exists = true;
                        break;
                    }
                }

                if(!exists) {
                    newFingerprint.scanResults.add(new CustomScanResult(existingBSSID, existingSSID, -100, 0));
                }
            }

            fingerprints.add(newFingerprint);

        }


        // AUSGEBEN
//        String ausgabe = "";
//        for(LocalizerBScanData sc: fingerprints) {
//            ausgabe += sc.
//        }

//        return null;
        return findMeNoDirection(currentScan, fingerprints, degree);

    }

    class FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF {
        public Long fingerprintId;
        public Double error;
        public int numberOfSameBSSIDs;
        public int maxPossibleNum;
        public int overlapPercentage;
        public double errorWithPercentage;

        public FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF(Long _fingerprintId , Double _error , int _numberOfSameBSSIDs , int _maxPossibleNum , int _overlapPercentage , double _errorWithPercentage) {
            fingerprintId = _fingerprintId;
            error = _error;
            numberOfSameBSSIDs = _numberOfSameBSSIDs;
            maxPossibleNum = maxPossibleNum;
            overlapPercentage = overlapPercentage;
            errorWithPercentage = errorWithPercentage;
        }
    }


//    public ArrayList<LocalizerBPositionResult> findMeNoDirection(List<ScanResult> currentScan, List<LocalizerBScanData> fingerprints, Float currentScanDirection) {
    public ArrayList<LocalizerBPositionResult> findMeNoDirection(List<CustomScanResult> currentScan, List<LocalizerBScanData> fingerprints, Float currentScanDirection) {
        Double currentMinDistA = Double.MAX_VALUE;
        Long bestFingerprintIdA = null;

        Double currentMinDistB =  Double.MAX_VALUE;
        Long bestFingerprintIdB = null;

        Double currentMinDistB1 =  Double.MAX_VALUE;
        Long bestFingerprintIdB1 = null;

        Double currentMinDistC =  Double.MAX_VALUE;
        Long bestFingerprintIdC = null;

        Double currentMinDistD =  Double.MAX_VALUE;
        Long bestFingerprintIdD = null;

        Double currentMinDistE =  Double.MAX_VALUE;
        Long bestFingerprintIdE = null;

//        let fingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF = []
        List<FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF> fingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF = new ArrayList<>();


        for(LocalizerBScanData fingerprint : fingerprints) {

            Double euclidianDistanceA = 0.0;
            Double euclidianDistanceB = 0.0;
            Double euclidianDistanceB1 = 0.0;
            Double euclidianDistanceC = 0.0;
            Double euclidianDistanceD = 0.0;
            Double euclidianDistanceE = 0.0;


            for(CustomScanResult scan : fingerprint.scanResults) {

                // wenn diese BSSID beim neuen scan in reichweite war: einen schritt für euklidische distanz berechnen
                boolean existsR = false;
                Integer cRSS = null;
                String cBSSID = null;

                // alle scans im neuen lokalisierungsfingerprint durchgehen
                for(CustomScanResult cnewscanitem : currentScan) {

                    if(cnewscanitem.getBSSID().equals(scan.getBSSID())) {
                        existsR = true;
                        cRSS = cnewscanitem.getLevel();
                        cBSSID = cnewscanitem.getBSSID();
                        break;
                    }
                }


                if(existsR) {
                    // CASE A
                    // console.log(scan.BSSID + ": " + scan.RSS + " AND " + cBSSID + ": " + cRSS);
                    int a = scan.getLevel();
                    int b = cRSS;
                    Double newPart = Math.pow((a-b), 2);
                    euclidianDistanceA += newPart;


                    // CASE C
                    int c = scan.getLevel();
                    int d = cRSS;
                    Double newPartC = Math.pow((c-d), 2);
                    euclidianDistanceC += newPartC;

                    // CASE E
                    int e = scan.getLevel();
                    int f = cRSS;
                    Double newPartE = Math.pow((e-f), 2);

                    Double identityModifier = 1.0;
                    if(e < -75 && f < -75) identityModifier = 0.5;
                    else if(e < -50 && f < -50) identityModifier = 0.5;
                    else if(e < -25 && f < -25) identityModifier = 0.75;
                    euclidianDistanceE += (newPartE * identityModifier);



                    // CASE B
                    // console.log("UNDEFINED?");
                    int a1 = scan.getLevel();
                    int b1 = cRSS;
                    Double newPartB = Math.pow((a1-b1), 2);
                    euclidianDistanceB += newPartB;



                    // CASE B1
                    // console.log("UNDEFINED?");
                    int a2 = scan.getLevel();
                    int b2 = cRSS;
                    Double newPartB1 = Math.pow((a2-b2), 2);
                    euclidianDistanceB1 += newPartB1;


                    // CASE D
                    int c1 = scan.getLevel();
                    int d1 = cRSS;
                    Double newPartD = Math.pow((c1-d1), 2);
                    euclidianDistanceD += newPartD;


                }
                else {
                    // console.error("OKAY");
                    // TODO: was machen mit BSSIDS die im neuen newScan nicht existieren? einfach auf -100 setzen?

                    //                  EUKD. ZU KORREKTEM FP   EUKD. ZU INKORRTEM FP
                    // OHNE DIESEN PART 14.2828568570857        41.352146256270665      DIFF: 27.069289399184965
                    // MIT  DIESEM PART 19.183326093250876      45.803929962395145      DIFF: 26.620603869144269
                    // => ohne diesen part ist die differenz größer -> ergebnis sollte besser sein -> diesen part weglassen (RICHTIG? TODO:)


                    // CASE B
                    // console.log("UNDEFINED?");
                    int a = scan.getLevel();
                    int b = -100;
                    Double newPart = Math.pow((a-b), 2);
                    euclidianDistanceB += newPart;

                    // CASE B1
                    // console.log("UNDEFINED?");
                    int a1 = scan.getLevel();
                    int b1 = -100;
                    Double newPartB1 = (Math.pow((a1-b1), 2))/2;
                    euclidianDistanceB1 += newPartB1;




                    // CASE D
                    int c = scan.getLevel();
                    int d = -100;
                    Double newPartD = Math.pow((c-d), 2);
                    euclidianDistanceD += newPartD;

                }

            }



            Float nOrMAlIzEdDirectionNow = currentScanDirection;
            if(nOrMAlIzEdDirectionNow > 180) {
                nOrMAlIzEdDirectionNow = (360 - nOrMAlIzEdDirectionNow);
            }

            Float norMALiZeDDirectioNFingerprint = fingerprint.direction;
            if(norMALiZeDDirectioNFingerprint > 180) {
                norMALiZeDDirectioNFingerprint = (360 - norMALiZeDDirectioNFingerprint);
            }



            // CASE C
            Float c1 = norMALiZeDDirectioNFingerprint;
            Float c2 = nOrMAlIzEdDirectionNow;
            Double newPartC1 = Math.pow((c1-c2), 2);
            euclidianDistanceC += newPartC1;


            // CASE D
            euclidianDistanceD += newPartC1;

            // CASE E
            euclidianDistanceE += newPartC1;




            euclidianDistanceA = Math.sqrt(euclidianDistanceA);
            euclidianDistanceB = Math.sqrt(euclidianDistanceB);
            euclidianDistanceB1 = Math.sqrt(euclidianDistanceB1);
            euclidianDistanceC = Math.sqrt(euclidianDistanceC);
            euclidianDistanceD = Math.sqrt(euclidianDistanceD);
            euclidianDistanceE = Math.sqrt(euclidianDistanceE);

            if(euclidianDistanceA < currentMinDistA) {
                currentMinDistA = euclidianDistanceA;
                bestFingerprintIdA = fingerprint.getUid();
            }

            if(euclidianDistanceB < currentMinDistB) {
                currentMinDistB = euclidianDistanceB;
                bestFingerprintIdB = fingerprint.getUid();
            }

            if(euclidianDistanceB1 < currentMinDistB1) {
                currentMinDistB1 = euclidianDistanceB1;
                bestFingerprintIdB1 = fingerprint.getUid();
            }

            if(euclidianDistanceC < currentMinDistC) {
                currentMinDistC = euclidianDistanceC;
                bestFingerprintIdC = fingerprint.getUid();
            }

            if(euclidianDistanceD < currentMinDistD) {
                currentMinDistD = euclidianDistanceD;
                bestFingerprintIdD = fingerprint.getUid();
            }

            if(euclidianDistanceE < currentMinDistE) {
                currentMinDistE = euclidianDistanceE;
                bestFingerprintIdE = fingerprint.getUid();
            }

            BSSIDIdOvercutInfo temp = findNumberOfSameBSSIDs(fingerprint.getUid(), currentScan, fingerprints);
            int overlapPercentage = (temp.numOfSameBssids/temp.maxPossibleNum)*100;
            int overlapPercentageInverse = 100 - overlapPercentage;
            FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF newObj = new FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF(fingerprint.getUid(), euclidianDistanceB, temp.numOfSameBssids, temp.maxPossibleNum, overlapPercentage, (euclidianDistanceB * overlapPercentage)  );
            fingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF.add(newObj);


//
//            fingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF.push({// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        fingerprintId: fingerprint.id,// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        error: euclidianDistanceB ,// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        numberOfSameBSSIDs: temp.numOfSameBssids,// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        maxPossibleNum: temp.maxPossibleNum,// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        overlapPercentage:overlapPercentage,                                                                                            // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                        errorWithPercentage: (euclidianDistanceB * overlapPercentage)                                                                   // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//                // errorWithPercentage: (overlapPercentageInverse)                                                                              // TODO: ggf nochmal probieren wenn man unidaten hat
//            })

        }

        // kleinsten fehlerwert mit prozent finden
        Double tempASmallestVal = Double.MAX_VALUE;
        Long tempAId = null;

        for(FingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF tempAA: fingerPrintIdsAndErrorsAndNumberOfSameBSSIDsF) {

            if(tempAA.errorWithPercentage < tempASmallestVal) {
                tempASmallestVal = tempAA.errorWithPercentage;
                tempAId = tempAA.fingerprintId;
            }

        }

        // fingerprint mit der id finden
        LocalizerBScanData foundFpA = null;
        LocalizerBScanData foundFpB = null;
        LocalizerBScanData foundFpB1 = null;
        LocalizerBScanData foundFpC = null;
        LocalizerBScanData foundFpD = null;
        LocalizerBScanData foundFpE = null;
        LocalizerBScanData foundFpF = null;


        for(LocalizerBScanData fingerprint : fingerprints) {
            if(fingerprint.getUid().equals(bestFingerprintIdA)) {
                foundFpA = fingerprint;
            }

            if(fingerprint.getUid().equals(bestFingerprintIdB)) {
                foundFpB = fingerprint;
            }

            if(fingerprint.getUid().equals(bestFingerprintIdB1)) {
                foundFpB1 = fingerprint;
            }


            if(fingerprint.getUid().equals(bestFingerprintIdC)) {
                foundFpC = fingerprint;
            }

            if(fingerprint.getUid().equals(bestFingerprintIdD)) {
                foundFpD = fingerprint;
            }

            if(fingerprint.getUid().equals(bestFingerprintIdE)) {
                foundFpE = fingerprint;
            }

            if(fingerprint.getUid().equals(tempAId )) {
                foundFpF = fingerprint;
            }


        }
        Double errorModifier = 30.0;

        ArrayList<LocalizerBPositionResult> results = new ArrayList<>();

        int grey =  Color.parseColor("#7F7A77");
        int olive =  Color.parseColor("#88BF76");
        int blue =  Color.parseColor("#35377F");
        int lightblue =  Color.parseColor("#2CD7E8");
        int purple =  Color.parseColor("#8C4CBF");
        int yellow =  Color.parseColor("#E5BA39");
        int orange = Color.parseColor("#E5632E");
        LocalizerBPositionResult lastCirlceF = new LocalizerBPositionResult(foundFpF.mapPosition, 70, grey, 0, "F");
        LocalizerBPositionResult lastCirlceA = new LocalizerBPositionResult(foundFpA.mapPosition, 60, olive, ((Double)(errorModifier * currentMinDistA)).intValue(), "A");
        LocalizerBPositionResult lastCirlceB = new LocalizerBPositionResult(foundFpB.mapPosition, 50, blue, ((Double)(errorModifier * currentMinDistB)).intValue(), "B");
        LocalizerBPositionResult lastCirlceB1 = new LocalizerBPositionResult(foundFpB1.mapPosition, 40, lightblue, ((Double)(errorModifier * currentMinDistB1)).intValue(), "B1");
        LocalizerBPositionResult lastCirlceC = new LocalizerBPositionResult(foundFpC.mapPosition, 30, orange, ((Double)(errorModifier * currentMinDistC)).intValue(), "C");
        LocalizerBPositionResult lastCirlceD = new LocalizerBPositionResult(foundFpD.mapPosition, 20, purple, ((Double)(errorModifier * currentMinDistD)).intValue(), "D");
        LocalizerBPositionResult lastCirlceE = new LocalizerBPositionResult(foundFpE.mapPosition, 10, yellow, ((Double)(errorModifier * currentMinDistE)).intValue(), "E");

        results.add(lastCirlceF);
        results.add(lastCirlceA);
        results.add(lastCirlceB);
        results.add(lastCirlceB1);
        results.add(lastCirlceC);
        results.add(lastCirlceD);
        results.add(lastCirlceE);

        return results;

    }


//    public BSSIDIdOvercutInfo findNumberOfSameBSSIDs(Long fingerprintid, List<ScanResult> currentScan, List<LocalizerBScanData> fingerprints) {
    public BSSIDIdOvercutInfo findNumberOfSameBSSIDs(Long fingerprintid, List<CustomScanResult> currentScan, List<LocalizerBScanData> fingerprints) {

        LocalizerBScanData foundFingerprint = null;
        for(LocalizerBScanData fingerprint : fingerprints) {

             if(fingerprint.getUid().equals(fingerprintid)) {
                 foundFingerprint = fingerprint;
                 break;
             }

        }

        int numOfSameBssids = 0;
        int maxPossibleNumFingerprintsBSSIDS = 0;

        for(CustomScanResult cScanB: foundFingerprint.scanResults) {
            String cBssId = cScanB.getBSSID();


            if(cScanB.getLevel() != -100) {
                maxPossibleNumFingerprintsBSSIDS++;
            }

            for(CustomScanResult newScanB : currentScan) {
                String bssidOther = newScanB.getBSSID();


                if(cBssId.equals(bssidOther) && cScanB.getLevel() != -100) {
                    numOfSameBssids++;
                }
            }
        }


        int maxPossibleNumnewscanBSSIDS = 0;
        for(CustomScanResult newScanB: currentScan) {
            String bssidOther = newScanB.getBSSID();
            maxPossibleNumnewscanBSSIDS++;
        }

        int maxPossibleNum = Math.max(maxPossibleNumnewscanBSSIDS, maxPossibleNumFingerprintsBSSIDS);

        return new BSSIDIdOvercutInfo(numOfSameBssids, maxPossibleNum);

    }

    class BSSIDIdOvercutInfo {
        public int numOfSameBssids;
        public int maxPossibleNum;
        public BSSIDIdOvercutInfo (int _numOfSameBssids, int _maxPossibleNum) {
            numOfSameBssids = _numOfSameBssids;
            maxPossibleNum = _maxPossibleNum;
        }
    }




}

