package com.example.marct.io_prototype2.wifiData;
//
//public class DatabaseHandler {
//}


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.marct.io_prototype2.wlanaccess.SortAccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.AccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.Scan;
import com.example.marct.io_prototype2.wlanaccess.model.Userscan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    final int rssNA = -100;
    final int maximumMacsTotal = 20;
    private List<Integer> fplist;
    private List<Fingerprint> fingerprints;
    private String scanDirection;

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "FingerprintData";

    public Context context;
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create data tables
        // db.execSQL("DROP TABLE IF EXISTS version");
        String CREATE_SSID_TABLE = "CREATE TABLE ssids ( "
                + "ssids_id INTEGER PRIMARY KEY, "
                + "ssids_name VARCHAR UNIQUE ON CONFLICT IGNORE, "
                + "ssids_trust INTEGER )";
        String CREATE_MAC_TABLE = "CREATE TABLE macs ( "
                + "macs_id INTEGER PRIMARY KEY, "
                + "macs_name VARCHAR UNIQUE ON CONFLICT IGNORE, "
                + "ssid_id INTEGER, "
                + "FOREIGN KEY(ssid_id) REFERENCES ssids(ssids_id))";
        String CREATE_RSS_TABLE = "CREATE TABLE rssdata ( "
                +
                // "id INTEGER AUTOINCREMENT, " +
                "fp_id INTEGER, " + "mac_id INTEGER, "
                + "orientation VARCHAR, " + "rss FLOAT, "
                + "PRIMARY KEY(fp_id,mac_id,orientation), "
                + "FOREIGN KEY(fp_id) REFERENCES fpdata(fpdata_id), "
                + "FOREIGN KEY(mac_id) REFERENCES mac(macs_id))";
        String CREATE_FPDATA_TABLE = "CREATE TABLE fpdata ( "
                + "fpdata_id INTEGER PRIMARY KEY, " + "fpdata_mapid INTEGER, "
                + "fpdata_xcoord FLOAT, " + "fpdata_ycoord FLOAT ) ";
        // + "fpdata_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP )";
        String CREATE_VERSION_TABLE = "CREATE TABLE version (timestamp TIMESTAMP DEFAULT '2014-01-01 00:00:00' PRIMARY KEY)";
        // create data tables
        db.execSQL(CREATE_SSID_TABLE);
        db.execSQL(CREATE_MAC_TABLE);
        db.execSQL(CREATE_RSS_TABLE);
        db.execSQL(CREATE_FPDATA_TABLE);
        db.execSQL(CREATE_VERSION_TABLE);
        Log.d("CREATE", "CREATE");
    }

    public void resetDB() {
        SQLiteDatabase db = this.getWritableDatabase();
		/*
		db.execSQL("DELETE FROM ssids");
		db.execSQL("DELETE FROM macs");
		db.execSQL("DELETE FROM rssdata");
		db.execSQL("DELETE FROM fpdata");
		*/
        db.execSQL("DROP TABLE IF EXISTS ssids");
        db.execSQL("DROP TABLE IF EXISTS macs");
        db.execSQL("DROP TABLE IF EXISTS rssdata");
        db.execSQL("DROP TABLE IF EXISTS fpdata");
        db.execSQL("DROP TABLE IF EXISTS version");
        this.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older data tables if existed
        db.execSQL("DROP TABLE IF EXISTS ssids");
        db.execSQL("DROP TABLE IF EXISTS macs");
        db.execSQL("DROP TABLE IF EXISTS rssdata");
        db.execSQL("DROP TABLE IF EXISTS fpdata");
        db.execSQL("DROP TABLE IF EXISTS version");

        // create fresh data tables
        this.onCreate(db);
        Log.d("UPGRADE", "UPGRADE");
    }

    public List<Integer> getFplist() {
        return fplist;
    }

    public List<Fingerprint> getFingerprints() {
        return fingerprints;
    }

    // ---------------------------------------------------------------------

    /**
     * CRUD operations (create "add", read "get", update, delete)
     */

    // table names
    private static final String TABLE_SSIDS = "ssids";
    private static final String TABLE_MACS = "macs";
    private static final String TABLE_RSSDATA = "rssdata";
    private static final String TABLE_FPDATA = "fpdata";
    private static final String TABLE_VERSION = "version";

    // table columns names
    private static final String KEY_SSIDS_ID = "ssids_id";
    private static final String KEY_SSIDS_NAME = "ssids_name";
    private static final String KEY_SSIDS_TRUST = "ssids_trust";
    private static final String KEY_MACS_ID = "macs_id";
    private static final String KEY_MACS_NAME = "macs_name";
    private static final String KEY_MACS_SSID = "ssid_id";
    // private static final String KEY_RSSDATA_ID = "id";
    private static final String KEY_RSSDATA_MACID = "mac_id";
    private static final String KEY_RSSDATA_FPID = "fp_id";
    private static final String KEY_RSSDATA_ORIENTATION = "orientation";
    private static final String KEY_RSSDATA_RSS = "rss";
    private static final String KEY_FPDATA_ID = "fpdata_id";
    private static final String KEY_FPDATA_MAPID = "fpdata_mapid";
    private static final String KEY_FPDATA_XCOORD = "fpdata_xcoord";
    private static final String KEY_FPDATA_YCOORD = "fpdata_ycoord";
    // private static final String KEY_FPDATA_TIMESTAMP = "fpdata_timestamp";
    private static final String KEY_VERSION_TIMESTAMP = "timestamp";

    private static final String[] SSID_COLUMNS = { KEY_SSIDS_ID,
            KEY_SSIDS_NAME, KEY_SSIDS_TRUST };
    private static final String[] MAC_COLUMNS = { KEY_MACS_ID, KEY_MACS_NAME,
            KEY_MACS_SSID };
    private static final String[] RSSDATA_COLUMNS = {
            /* KEY_RSSDATA_ID, */KEY_RSSDATA_MACID, KEY_RSSDATA_FPID,
            KEY_RSSDATA_ORIENTATION, KEY_RSSDATA_RSS };
    private static final String[] FPDATA_COLUMNS = { KEY_FPDATA_ID,
            KEY_FPDATA_MAPID, KEY_FPDATA_XCOORD, KEY_FPDATA_YCOORD };
    // KEY_FPDATA_TIMESTAMP };
    private static final String[] VERSION_COLUMNS = { KEY_VERSION_TIMESTAMP };

    public List<Fingerprint> getAllFp(int mapid) {
        fingerprints = new ArrayList<Fingerprint>();
        Fingerprint fp;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " + KEY_FPDATA_XCOORD+","+KEY_FPDATA_YCOORD+","+KEY_FPDATA_MAPID+","+KEY_FPDATA_ID
                + " FROM " + TABLE_FPDATA;
        if (mapid != 0) {
            query += " WHERE " + KEY_FPDATA_MAPID + "=" + mapid;
        }

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {

                fp = new Fingerprint();
                fp.setMapid(cursor.getInt(cursor.getColumnIndex(KEY_FPDATA_MAPID)));
                fp.setFpid(cursor.getInt(cursor.getColumnIndex(KEY_FPDATA_ID)));
                fp.setXcoord(cursor.getFloat(cursor.getColumnIndex(KEY_FPDATA_XCOORD)));
                fp.setYcoord(cursor.getFloat(cursor.getColumnIndex(KEY_FPDATA_YCOORD)));
                fingerprints.add(fp);
            } while (cursor.moveToNext());
        }

        db.close();
        return fingerprints;
    }

    public void addFP(Scan fp) {
        // Log.d("addFP");
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues fpdata_values = new ContentValues();
//
//                                                fpdata_values.put(KEY_FPDATA_MAPID, 0); // get mapid
//                                                fpdata_values.put(KEY_FPDATA_XCOORD, 0); // get x coordinate
//                                                fpdata_values.put(KEY_FPDATA_YCOORD, 0); // get y coordinate

                                                                        // TODO: ????? das oben war das original, warum war das alles auf 0?
                                                fpdata_values.put(KEY_FPDATA_MAPID, fp.getfPrint().mapid); // get mapid
                                                fpdata_values.put(KEY_FPDATA_XCOORD, fp.getfPrint().xcoord); // get x coordinate
                                                fpdata_values.put(KEY_FPDATA_YCOORD, fp.getfPrint().ycoord); // get y coordinate


        long fpid = db.insertWithOnConflict(TABLE_FPDATA, null, fpdata_values,
                0);
        if (fpid == -1)
            Log.d(TABLE_FPDATA, "FPid error");

        for (int i = 0; i < fp.getAllAps().size(); i++) {
            ContentValues ssid_values = new ContentValues();
            ssid_values.put(KEY_SSIDS_NAME, fp.getAllAps().get(i).getSsid()); // get
            // ssid
            long ssidid = db.insertWithOnConflict(TABLE_SSIDS, null,
                    ssid_values, 0);

            if (ssidid == -1) {
                Log.d(TABLE_SSIDS, "ssidID error");
                String query = "SELECT " + KEY_SSIDS_ID + " FROM "
                        + TABLE_SSIDS + " WHERE " + KEY_SSIDS_NAME + "='"
                        + ssid_values.get(KEY_SSIDS_NAME) + "'";
                Cursor cursor = db.rawQuery(query, null);
                if (cursor != null)
                    cursor.moveToFirst();
                ssidid = cursor.getInt(cursor.getColumnIndex(KEY_SSIDS_ID));
            }
            Log.d(TABLE_SSIDS, "SSID ID:" + Long.toString(ssidid));

            ContentValues mac_values = new ContentValues();
            mac_values.put(KEY_MACS_NAME, fp.getAllAps().get(i).getMacAdress()); // get
            // mac
            Log.d(TABLE_MACS, fp.getAllAps().get(i).getMacAdress());
            mac_values.put(KEY_MACS_SSID, ssidid); // get mac
            long macid = db.insertWithOnConflict(TABLE_MACS, null, mac_values,
                    0);

            if (macid == -1) {
                Log.d(TABLE_MACS, "macID error");
                String query = "SELECT " + KEY_MACS_ID + " FROM " + TABLE_MACS
                        + " WHERE " + KEY_MACS_NAME + "='"
                        + mac_values.get(KEY_MACS_NAME) + "'";
                Cursor cursor = db.rawQuery(query, null);
                if (cursor != null)
                    cursor.moveToFirst();
                macid = cursor.getInt(cursor.getColumnIndex(KEY_MACS_ID));
            }
            Log.d(TABLE_MACS, "MAC ID:" + Long.toString(macid));

            ContentValues rssdata_values = new ContentValues();
            rssdata_values.put(KEY_RSSDATA_MACID, macid); // ...
            rssdata_values.put(KEY_RSSDATA_FPID, fpid); // ...
            rssdata_values.put(KEY_RSSDATA_ORIENTATION, fp.getDirection()); // get
            // orientation
            rssdata_values.put(KEY_RSSDATA_RSS, fp.getAllAps().get(i)
                    .getAverageSignal()); // get rss

            db.insert(TABLE_RSSDATA, null, rssdata_values);
        }

        // 4. close
        db.close();
    }

    public void updateDB(JSONObject DBinput, JSONObject DBversion) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*
         * db.execSQL("DELETE FROM ssids"); db.execSQL("DELETE FROM macs");
         * db.execSQL("DELETE FROM rssdata"); db.execSQL("DELETE FROM fpdata");
         */
        db.execSQL("DROP TABLE IF EXISTS ssids");
        db.execSQL("DROP TABLE IF EXISTS macs");
        db.execSQL("DROP TABLE IF EXISTS rssdata");
        db.execSQL("DROP TABLE IF EXISTS fpdata");
        db.execSQL("DROP TABLE IF EXISTS version");

        // create fresh data tables
        this.onCreate(db);
        db.beginTransaction();

        ContentValues fpdata_values = new ContentValues();
        ContentValues ssid_values = new ContentValues();
        ContentValues mac_values = new ContentValues();
        ContentValues rssdata_values = new ContentValues();
        ContentValues version_values = new ContentValues();

        try {
            JSONArray allData = DBinput.getJSONArray("tables");
            // JSONObject timestamp=DBversion.getJSONArray("timestamp");
            version_values.put(KEY_VERSION_TIMESTAMP,
                    DBversion.getString("timestamp"));
            db.insert(TABLE_VERSION, null, version_values);

            JSONObject tables = allData.getJSONObject(0);
            JSONArray ssids = tables.getJSONArray("SSIDs");
            for (int i = 0; i < ssids.length(); i++) {
                JSONObject record = ssids.getJSONObject(i);
                ssid_values.put(KEY_SSIDS_NAME, record.getString("SSID"));
                ssid_values.put(KEY_SSIDS_ID, record.getString("id"));
                ssid_values.put(KEY_SSIDS_TRUST, record.getString("Trust"));
                db.insert(TABLE_SSIDS, null, ssid_values);
            }
            Log.d("DB Handler", "SSIDs inserted");
            JSONArray macs = tables.getJSONArray("MACs");
            for (int i = 0; i < macs.length(); i++) {
                JSONObject record = macs.getJSONObject(i);
                mac_values.put(KEY_MACS_NAME, record.getString("MAC"));
                mac_values.put(KEY_MACS_ID, record.getString("id"));
                mac_values.put(KEY_MACS_SSID, record.getString("idSSID"));
                db.insert(TABLE_MACS, null, mac_values);
            }
            Log.d("DB Handler", "MACs inserted");
            JSONArray fpdata = tables.getJSONArray("Messpunkte");
            for (int i = 0; i < fpdata.length(); i++) {
                JSONObject record = fpdata.getJSONObject(i);
                fpdata_values.put(KEY_FPDATA_ID, record.getString("id"));
                fpdata_values.put(KEY_FPDATA_MAPID,
                        record.getString("Karte_idKarte"));
                fpdata_values.put(KEY_FPDATA_XCOORD,
                        record.getString("x_Koord"));
                fpdata_values.put(KEY_FPDATA_YCOORD,
                        record.getString("y_Koord"));
                // fpdata_values.put(KEY_FPDATA_TIMESTAMP,
                // record.getString("Timestamp"));
                db.insert(TABLE_FPDATA, null, fpdata_values);
            }
            Log.d("DB Handler", "FPs inserted");
            JSONArray rssdata = tables.getJSONArray("Signalstaerken");
            for (int i = 0; i < rssdata.length(); i++) {
                JSONObject record = rssdata.getJSONObject(i);
                rssdata_values
                        .put(KEY_RSSDATA_MACID, record.getString("idMAC"));
                rssdata_values.put(KEY_RSSDATA_FPID,
                        record.getString("idMesspunkt"));
                rssdata_values.put(KEY_RSSDATA_ORIENTATION,
                        record.getString("Richtung"));
                rssdata_values.put(KEY_RSSDATA_RSS,
                        record.getString("Signalstaerke"));
                db.insert(TABLE_RSSDATA, null, rssdata_values);
            }
            Log.d("DB Handler", "RSS inserted");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        db.setTransactionSuccessful();
        db.endTransaction();

        db.close();
    }

    public Fingerprint getFP(int fpid) {
        Fingerprint fp = null;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " + KEY_FPDATA_XCOORD + "," + KEY_FPDATA_YCOORD
                + "," + KEY_FPDATA_MAPID + " " + "FROM " + TABLE_FPDATA
                + " WHERE " + KEY_FPDATA_ID + "=" + fpid;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            fp = new Fingerprint();
            fp.setMapid(cursor.getInt(cursor.getColumnIndex(KEY_FPDATA_MAPID)));
            fp.setXcoord(cursor.getFloat(cursor
                    .getColumnIndex(KEY_FPDATA_XCOORD)));
            fp.setYcoord(cursor.getFloat(cursor
                    .getColumnIndex(KEY_FPDATA_YCOORD)));
        }
        // read values from database table FPDATA
        // store values in Fingerprint object
        // return Fingerprint object
        db.close();
        return fp;
    }

    public String getVersion() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + KEY_VERSION_TIMESTAMP + " FROM "
                + TABLE_VERSION;

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            String version = cursor.getString(0);
            Log.d("Versionsvergleich", version);
            db.close();
            return version;
        } else
            // read values from database table FPDATA
            // store values in Fingerprint object
            // return Fingerprint object
            db.close();
        return null;
    }

                                                                                //    public boolean getPosition(Scan measure) { // TODO: old
    public boolean getPosition(Userscan measure) {
//

        SQLiteDatabase db = this.getReadableDatabase();
        // int macsTotal = measure.getAllAps().size();
        int macsTotal = maximumMacsTotal;
        if (measure.getAllAps().size() < macsTotal)
            macsTotal = measure.getAllAps().size();
        Log.d("DBHandler", "macsTotal: " + macsTotal);

        scanDirection=measure.getDirection();
        Log.d("DBHandler", "scan direction: " + scanDirection);

        String macs = TABLE_MACS + "." + KEY_MACS_NAME + "='00:00:00:00:00:00'";
        for (int i = 0; i < macsTotal; i++) {
            if (i != 0) {
                macs += " OR ";
            } else
                macs = "";

            macs += TABLE_MACS + "." + KEY_MACS_NAME + "='"
                    + measure.getAllAps().get(i).getMacAdress() + "'";
            Log.d("DBHandler", "Mess mac:"
                    + measure.getAllAps().get(i).getMacAdress());
        }
        Log.d("DBHandler", "Query macs: " + macs);

        String query1 = "SELECT DISTINCT " + TABLE_MACS + "." + KEY_MACS_ID
                + "," + TABLE_MACS + "." + KEY_MACS_NAME + " " + "FROM "
                + TABLE_MACS + ", " + TABLE_RSSDATA + " " + "WHERE (" + macs
                + ") AND " + TABLE_RSSDATA + "." + KEY_RSSDATA_MACID + "="
                + TABLE_MACS + "." + KEY_MACS_ID + " ORDER BY " + KEY_MACS_ID;

        String query = "SELECT " + TABLE_RSSDATA + "." + KEY_RSSDATA_FPID
                + ", " + TABLE_RSSDATA + "." + KEY_RSSDATA_MACID + ", "
                + TABLE_RSSDATA + "." + KEY_RSSDATA_RSS + ", " + TABLE_RSSDATA
                + "." + KEY_RSSDATA_ORIENTATION + " " + "FROM " + TABLE_RSSDATA
                + ", " + TABLE_MACS + " " + "WHERE (" + macs + ") AND "
                + TABLE_RSSDATA + "." + KEY_RSSDATA_MACID + "=" + TABLE_MACS
                + "." + KEY_MACS_ID;

        fplist = new ArrayList<Integer>();

        Cursor cursor = db.rawQuery(query1, null);

        if (!cursor.moveToFirst()) {
            Log.d("DBHandler", "shortQuery no result");
            return false;
        }

        ArrayList<Integer> sortedRSS = new ArrayList<Integer>();
        ArrayList<Integer> sortedMacID = new ArrayList<Integer>();

        do {
            String macname = cursor.getString(cursor
                    .getColumnIndex(KEY_MACS_NAME));
            int macid = cursor.getInt(cursor.getColumnIndex(KEY_MACS_ID));

            for (int i = 0; i < macsTotal; i++) {
                if (macname.equals(measure.getAllAps().get(i).getMacAdress())) {
                    sortedRSS
                            .add(measure.getAllAps().get(i).getAverageSignal());
					/*
					Log.d("DBHandler", "Found Mac: " + macname + ", RSS: "
							+ measure.getAllAps().get(i).getAverageSignal());
					*/
                }

            }
            sortedMacID.add(macid);
        } while (cursor.moveToNext());
        macsTotal = sortedMacID.size();

        cursor = db.rawQuery(query, null);

        if (!cursor.moveToFirst()) {
            Log.d("DBHandler", "longQuery no result");
            return false;
        }

        int fpid = cursor.getInt(cursor.getColumnIndex(KEY_RSSDATA_FPID));
        int macid = cursor.getInt(cursor.getColumnIndex(KEY_RSSDATA_MACID));
        String orientation = cursor.getString(cursor
                .getColumnIndex(KEY_RSSDATA_ORIENTATION));
        int rss = cursor.getInt(cursor.getColumnIndex(KEY_RSSDATA_RSS));

        int macidpos = -1;
        for (int i = 0; i < macsTotal; i++) {
            if (macid == sortedMacID.get(i)) {
                macidpos = i;
                // Log.d("DB macidpos", "1. macidpos: " + macidpos);
                break;
            }
        }

        ArrayList<Integer[][]> konstrukt = new ArrayList<Integer[][]>();
        Integer[][] fpArray = new Integer[macsTotal][4];
        for (int i = 0; i < macsTotal; i++) {
            Arrays.fill(fpArray[i], rssNA);
        }

        do {
            rss = cursor.getInt(cursor.getColumnIndex(KEY_RSSDATA_RSS));
            orientation = cursor.getString(cursor
                    .getColumnIndex(KEY_RSSDATA_ORIENTATION));

            if (macid != cursor
                    .getInt(cursor.getColumnIndex(KEY_RSSDATA_MACID))
                    || fpid != cursor.getInt(cursor
                    .getColumnIndex(KEY_RSSDATA_FPID))) {
                if (fpid != cursor.getInt(cursor
                        .getColumnIndex(KEY_RSSDATA_FPID))) {
                    fplist.add(fpid);
                    konstrukt.add(fpArray);
                    fpArray = new Integer[macsTotal][4];
                    for (int i = 0; i < macsTotal; i++) {
                        Arrays.fill(fpArray[i], rssNA);
                    }

                    fpid = cursor.getInt(cursor
                            .getColumnIndex(KEY_RSSDATA_FPID));
                }
                macid = cursor.getInt(cursor.getColumnIndex(KEY_RSSDATA_MACID));

                for (int i = 0; i < macsTotal; i++) {
                    if (macid == sortedMacID.get(i)) {
                        macidpos = i;
                        // Log.d("DB macidpos", "macidpos: " + macidpos);
                        break;
                    }
                }
            }

            if (orientation.equals("Nord")) {
                fpArray[macidpos][0] = rss;
            } else if (orientation.equals("Ost")) {
                fpArray[macidpos][1] = rss;
            } else if (orientation.equals("Sued")) {
                fpArray[macidpos][2] = rss;
            } else if (orientation.equals("West")) {
                fpArray[macidpos][3] = rss;
            }

        } while (cursor.moveToNext());

        konstrukt.add(fpArray);
        fplist.add(fpid);

        int countFPs = 0;
        int countNA = 0;
        for (int i = 0; i < konstrukt.size(); i++) {
            for (int m = 0; m < fpArray.length; m++) {
                if (konstrukt.get(i)[m][0] == 0) {
                    countNA++;
                    // Log.d("DB Handler", "test123");
                } else {
/*
					Log.d("DB macPosAdd",
							"rss " + konstrukt.get(i)[m][0].toString() + ","
									+ konstrukt.get(i)[m][1].toString() + ","
									+ konstrukt.get(i)[m][2].toString() + ","
									+ konstrukt.get(i)[m][3].toString()
									+ ", fpid:" + fplist.get(i) + ", macid:"
									+ sortedMacID.get(m));
*/
                }
            }
            countFPs++;
            //Log.d("DB macPosAdd", "nextfp");
        }

        // Lokalisierung

        // Log.i("DB","sortedRSS "+sortedRSS.size()+" sortedMacID "+sortedMacID+" konstrukt "+konstrukt.size());

        //meanDirectionAlgo(macsTotal, konstrukt, sortedRSS);
        //allDirectionsAlgo(macsTotal, konstrukt, sortedRSS);
        singleDirectionAlgo(macsTotal, konstrukt, sortedRSS);

        Log.d("DB", "Dir " + measure.getDirection());

        db.close();
        return true;
    }

    public void meanDirectionAlgo(int macsTotal, ArrayList<Integer[][]> konstrukt,
                                  ArrayList<Integer> sortedRSS) {

        HashMap<Integer, Integer> distanceMap = new HashMap<Integer, Integer>();

        for (int ifp = 0; ifp < konstrukt.size(); ifp++) {

            float distance = 0;

            for (int imac = 0; imac < macsTotal; imac++) {

                float meanRss = 0;

                for (int iotn = 0; iotn < 4; iotn++) {
                    meanRss += konstrukt.get(ifp)[imac][iotn];
                }
                distance += Math.pow((sortedRSS.get(imac) - meanRss / 4), 2);
            }

            distanceMap.put(fplist.get(ifp), (int) distance);
        }
        Map sortedMap = sortByValue(distanceMap);
        Log.d("DB", "sorted" + sortedMap);
        ArrayList<Integer> inverseList = new ArrayList<Integer>(
                sortedMap.keySet());
        Collections.reverse(inverseList);
        Log.d("DB", "inverted" + inverseList);

        fplist=inverseList;
        Log.d("DB", "fplist" + fplist);
    }

    public void allDirectionsAlgo(int macsTotal, ArrayList<Integer[][]> konstrukt,
                                  ArrayList<Integer> sortedRSS) {

        HashMap<String, Integer> distanceMap = new HashMap<String, Integer>();

        for (int ifp = 0; ifp < konstrukt.size(); ifp++) {

            float distanceN = 0;
            float distanceE = 0;
            float distanceS = 0;
            float distanceW = 0;

            for (int imac = 0; imac < macsTotal; imac++) {

                float RssN = konstrukt.get(ifp)[imac][0];
                float RssE = konstrukt.get(ifp)[imac][1];
                float RssS = konstrukt.get(ifp)[imac][2];
                float RssW = konstrukt.get(ifp)[imac][3];
                distanceN += Math.pow((sortedRSS.get(imac) - RssN), 2);
                distanceE += Math.pow((sortedRSS.get(imac) - RssE), 2);
                distanceS += Math.pow((sortedRSS.get(imac) - RssS), 2);
                distanceW += Math.pow((sortedRSS.get(imac) - RssW), 2);

            }

            if (distanceMap.put(fplist.get(ifp).toString() + "N",
                    (int) distanceN) != null)
                Log.d("DB", "key replaced");
            if (distanceMap.put(fplist.get(ifp).toString() + "E",
                    (int) distanceE) != null)
                Log.d("DB", "key replaced");
            if (distanceMap.put(fplist.get(ifp).toString() + "S",
                    (int) distanceS) != null)
                Log.d("DB", "key replaced");
            if (distanceMap.put(fplist.get(ifp).toString() + "W",
                    (int) distanceW) != null)
                Log.d("DB", "key replaced");
        }
        Map sortedMap = sortByValue(distanceMap);
        Log.d("DB", "sorted" + sortedMap);

        ArrayList<String> inverseList = new ArrayList<String>(
                sortedMap.keySet());
        Collections.reverse(inverseList);
        // Log.d("DB","inverted"+inverseList);
        fplist = new ArrayList<Integer>();
        for (int i = 0; i < inverseList.size(); i++) {
            fplist.add(Integer.parseInt(inverseList.get(i).substring(0,
                    inverseList.get(i).length() - 1)));
        }

        Log.d("DB", "fplist" + fplist);
    }

    public void singleDirectionAlgo(int macsTotal, ArrayList<Integer[][]> konstrukt,
                                    ArrayList<Integer> sortedRSS) {

        HashMap<Integer, Integer> distanceMap = new HashMap<Integer, Integer>();

        int dir=0;

        if(scanDirection.equals("North"))
            dir=0;
        else if(scanDirection.equals("East"))
            dir=1;
        else if(scanDirection.equals("South"))
            dir=2;
        else if(scanDirection.equals("West"))
            dir=3;

        Log.d("DB", "dir "+dir);

        for (int ifp = 0; ifp < konstrukt.size(); ifp++) {

            float distance = 0;

            for (int imac = 0; imac < macsTotal; imac++) {

                float Rss = konstrukt.get(ifp)[imac][dir];
                distance += Math.pow((sortedRSS.get(imac) - Rss), 2);


            }

            if (distanceMap.put(fplist.get(ifp),(int) distance) != null)
                Log.d("DB", "key replaced");

        }

        Map sortedMap = sortByValue(distanceMap);
        Log.d("DB", "sorted" + sortedMap);
        ArrayList<Integer> inverseList = new ArrayList<Integer>(
                sortedMap.keySet());
        Collections.reverse(inverseList);
        Log.d("DB", "inverted" + inverseList);

        fplist=inverseList;
        Log.d("DB", "fplist" + fplist);
    }

    public static Map sortByValue(Map unsortedMap) {
        Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
        sortedMap.putAll(unsortedMap);
        return sortedMap;
    }

}

class ValueComparator implements Comparator {

    Map map;

    public ValueComparator(Map map) {
        this.map = map;
    }

    public int compare(Object keyA, Object keyB) {
        Comparable valueA = (Comparable) map.get(keyA);
        Comparable valueB = (Comparable) map.get(keyB);
        return valueB.compareTo(valueA);
    }
}

