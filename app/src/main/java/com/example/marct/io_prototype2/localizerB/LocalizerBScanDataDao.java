package com.example.marct.io_prototype2.localizerB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import java.util.List;

@Dao
public interface LocalizerBScanDataDao {

    @Query("SELECT * FROM LocalizerBScanData")
    List<LocalizerBScanData> getAll();

    @Insert
    void insertAll(LocalizerBScanData... fingerprints);

    @Query("DELETE FROM LocalizerBScanData")
    void deleteAll();

}
