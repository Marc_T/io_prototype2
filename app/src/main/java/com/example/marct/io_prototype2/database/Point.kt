package com.example.marct.io_prototype2.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Point(
        @PrimaryKey(autoGenerate = true) var uid: Long?,
        @ColumnInfo(name="magnetic_x") var magneticX: Double? = null,
        @ColumnInfo(name="magnetic_y") var magneticY: Double? = null,
        @ColumnInfo(name="magnetic_z") var magneticZ: Double? = null,
        @ColumnInfo(name="magnetic_magnitude") var magneticMagnitude: Double? = null,
        @ColumnInfo(name="screen_x") var screenX: Float? = null,
        @ColumnInfo(name="screen_y") var screenY: Float? = null) {

    constructor():this(null)
}