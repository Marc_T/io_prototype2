package com.example.marct.io_prototype2.localizerB

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.net.wifi.ScanResult
import com.example.marct.io_prototype2.MapPosition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import java.util.Arrays.asList
import java.util.Arrays.asList







@Entity
data class LocalizerBScanData(
        @PrimaryKey(autoGenerate = true) var uid: Long?,
        @ColumnInfo(name="direction") @JvmField var direction: Float? = null,
        @ColumnInfo(name="mapPosition") @JvmField var mapPosition: MapPosition? = null,
//        @ColumnInfo(name="scanResults") @JvmField var scanResults: String? = null) {
    @ColumnInfo(name="scanResults") @JvmField var scanResults: ArrayList<CustomScanResult>? = null) {
//    @ColumnInfo(name="scanResults") @JvmField var scanResults: ScanResult? = null) {

    constructor():this(null)

    override fun toString(): String {
        return " Posi: " + mapPosition;
    }

}

@Entity // NOTE TODO: same as class above just other name
data class LocalizerBTestData(
        @PrimaryKey(autoGenerate = true) var uid: Long?,
        @ColumnInfo(name="direction") @JvmField var direction: Float? = null,
        @ColumnInfo(name="mapPosition") @JvmField var mapPosition: MapPosition? = null,
        @ColumnInfo(name="scanResults") @JvmField var scanResults: ArrayList<CustomScanResult>? = null) {

    constructor():this(null)

    override fun toString(): String {
        return " Posi: " + mapPosition;
    }

}