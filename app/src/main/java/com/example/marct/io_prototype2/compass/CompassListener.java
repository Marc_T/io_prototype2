package com.example.marct.io_prototype2.compass;

import com.example.marct.io_prototype2.CanvasView;
import com.example.marct.io_prototype2.MainActivity;
import com.example.marct.io_prototype2.localizerB.localizerBScanActivity;
import com.example.marct.io_prototype2.wlanaccess.model.Scan;

import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;

public class CompassListener implements SensorEventListener {


    private SensorManager mSensorManager;
    localizerBScanActivity activity;
    public float degree;

    private MainActivity mainactivity = null;

    public CompassListener(SensorManager mSensorManager){
        this.mSensorManager = mSensorManager;
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    public void setMainactivity(MainActivity _mainactivity) {
        mainactivity = _mainactivity;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // get the angle around the z-axis rotated
        degree = Math.round(event.values[0]);


        if(activity != null) {
            activity.setDegree(degree);
        }
        if(mainactivity != null) {
            mainactivity.setDegree(degree);
        }
    }

    public void setActivity(localizerBScanActivity a) {
        activity = a;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

    public Float getOrientationFloat() {
        return degree;
    }

    public String getOrientation(){

        String orientation = "";

        if(degree < 45 || degree >= 315)
            orientation = Scan.NORTH;
        else if(degree >= 45 && degree < 135)
            orientation = Scan.EAST;
        else if(degree >= 135 && degree < 225)
            orientation = Scan.SOUTH;
        else if(degree >= 225 && degree < 315)
            orientation = Scan.WEST;

        return orientation;
    }


}
