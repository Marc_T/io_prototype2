package com.example.marct.io_prototype2;

import android.companion.WifiDeviceFilter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.aware.WifiAwareManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.content.BroadcastReceiver;

import com.example.marct.io_prototype2.compass.CompassListener;
import com.example.marct.io_prototype2.wifiData.DatabaseHandler;
import com.example.marct.io_prototype2.wifiData.Fingerprint;
import com.example.marct.io_prototype2.wlanaccess.SortAccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.AccessPoint;
import com.example.marct.io_prototype2.wlanaccess.model.Userscan;
//import com.example.marct.io_prototype2.wlanaccess.model.Scan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.content.Context.CARRIER_CONFIG_SERVICE;
import static android.content.Context.SENSOR_SERVICE;

public class LocalizerFHDMapsJAVA {

    public LocalizerFHDMapsJAVA(Context _context, WifiManager _wfman, CanvasView _canvasview) {
        context = _context;
        wifiManager = _wfman;
//        sm = (SensorManager) context
//                .getSystemService(SENSOR_SERVICE);
//        cl = new CompassListener(sm);
     dbHandler = new DatabaseHandler(context);
     canvasview = _canvasview;
    }

    CanvasView canvasview;
    Context context;
    List<ScanResult> wifiList;
    WifiManager wifiManager;
    HashMap<String, AccessPoint> allAps;
    DatabaseHandler dbHandler;
    private ArrayList<AccessPoint> orderedAps;

//    SensorManager sm;
//    CompassListener cl;

    String lastScanOutput = "";



    private ArrayList<AccessPoint> orderAps(HashMap<String, AccessPoint> allAps) {
        ArrayList<AccessPoint> orderedAps = new ArrayList<AccessPoint>();

        orderedAps.addAll(allAps.values());

        Collections.sort(orderedAps,
                Collections.reverseOrder(new SortAccessPoint()));

        return orderedAps;
    }

//    public void startAutomaticFindMe(CanvasView canvasView) {
////        mainActivity = mainActivity
//        Log.d("FT3", "Started automatic");
//        WifiReceiver wifiReceiver = new WifiReceiver(canvasView, wifiManager);
//
//
//        canvasView.mainactivity.registerReceiver(wifiReceiver, new IntentFilter(
//                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//
//    }

    public Float[] findMe(List<ScanResult> newWifiList, String orientation) {

//        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

//        wifiList = wifiManager.getScanResults();

        wifiList = newWifiList;


        allAps = new HashMap<String, AccessPoint>();

        String currentScanOutput = "";

        for (ScanResult sr : wifiList) {
//                                                                                              // TODO: FUNKTIONIERT DAS?
                                                                                                sr.SSID = Utility.removeSpecialCharacters(sr.SSID);
            Log.v("ScanAct", "Scan: " + sr.BSSID + ", " + sr.level);

            currentScanOutput += sr.BSSID + ": " + sr.level + " ";


            if (!allAps.containsKey(sr.BSSID)) {
                allAps.put(sr.BSSID, new AccessPoint(sr.level, sr.SSID,
                        sr.BSSID, sr.frequency));
            } else {
                allAps.get(sr.BSSID).addSignal(sr.level);
            }




        }
//
//        boolean scanresultsarethesame = false;
//        String a = "Last scan results are NOT the same";
//        if(lastScanOutput.equals(currentScanOutput)) {
//            scanresultsarethesame = true;
//             a = "Last scan results ARE the same";
//        }
//        canvasview.setCurrentScanText(a);



        lastScanOutput = currentScanOutput;


        orderedAps = orderAps(allAps);

//        Userscan scan = new Userscan(cl.getOrientation(), orderedAps);
        Userscan scan = new Userscan(orientation, orderedAps);
        Log.v("ScanAct", "scanObj: " + scan.getAllAps().size());
        Fingerprint fp;
        dbHandler.getPosition(scan);
        if (dbHandler.getFplist().size() > 0) {
            fp = dbHandler.getFP(dbHandler.getFplist().get(0));
            Log.d("fp",
                    fp.getMapid() + "," + fp.getXcoord() + ","
                            + fp.getYcoord());

//            return fp;
//            return fp.getXcoord();
            Float[] res = new Float[2];
            res[0] = fp.getXcoord();
            res[1] = fp.getYcoord();
            return res;

//            int level = MapFragment.searchFloor(fp.getMapid()).getLevel();
//            mapFragment.levelListener.setColors(level);
//            if (findme) {
//                mView.setMarker(fp.getXcoord(), fp.getYcoord(), level, 1);
//                findme = false;
//            } else
//                mView.moveMarker(fp.getXcoord(), fp.getYcoord(), level, 1);
        } else {
            String message = "Leider konnte Ihre Position nicht ermittelt werden";
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            // Alertdialog zusammenbauen
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                    MainActivity.this);
//            alertDialogBuilder
//                    .setTitle("Lokalisierung fehlgeschlagen")
//                    .setMessage(message)
//                    .setCancelable(false)
//                    .setPositiveButton("Ok",
//                            new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog,
//                                                    int id) {
//                                    dialog.cancel();
//                                }
//                            });
            return null;
//
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
        }

    }



    // automatisch lokalisieren, nachdem button dafür gedrückt wurde
//    class WifiReceiver extends BroadcastReceiver {
////        final Context context = getApplicationContext();
////        List<ScanResult> wifiList;
////        HashMap<String, AccessPoint> allAps;
////        DatabaseHandler dbHandler = new DatabaseHandler(context);
////        private ArrayList<AccessPoint> orderedAps;
////
////        SensorManager sm = (SensorManager) context
////                .getSystemService(SENSOR_SERVICE);
////        CompassListener cl = new CompassListener(sm);
//
//
//
//        CanvasView canvasView = null;
//
//        WifiManager wifiManager = null;
//
//        public WifiReceiver(CanvasView _canvasView, WifiManager wifiManager) {
//            canvasView = _canvasView;
//            wifiManager = wifiManager;
//            wifiManager.startScan();
//
//            Log.d("FT3", "Started automatic2");
//
//        }
//
//        public void restartScan() {
//
//
////            Thread t = new Thread() {
////
////                @Override
////                public void run() {
////                    try {
////
////
////
////                        this.wait(2000);
////
//////                        canvasView.mainactivity.registerReceiver(wifiManager, new IntentFilter(
//////                                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
////
////                        wifiManager.startScan();
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
////
////
////                }
////
////            };
////            t.start();
//            wifiManager.startScan();
//
//        }
//
//
//                        public void onReceive(Context c, Intent intent) {
//                            Log.d("FT3", "ONRECEIVE");
//
//                            if(wifiManager == null) {
//                                Log.d("FT3", "NPE2 triggered");
//                                wifiManager = (WifiManager) c.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//                            }
//
//                //            canvasView.setCurrentScanText();
//                            wifiList = wifiManager.getScanResults();
//
//                            allAps = new HashMap<String, AccessPoint>();
//
//                            for (ScanResult sr : wifiList) {
//                                Log.v("ScanAct", "Scan: " + sr.BSSID + ", " + sr.level);
//                                if (!allAps.containsKey(sr.BSSID)) {
//                                    allAps.put(sr.BSSID, new AccessPoint(sr.level, sr.SSID,
//                                            sr.BSSID, sr.frequency));
//                                } else {
//                                    allAps.get(sr.BSSID).addSignal(sr.level);
//                                }
//                            }
//                            orderedAps = orderAps(allAps);
//
//                            Userscan scan = new Userscan (cl.getOrientation(), orderedAps);
//                            Log.v("ScanAct", "scanObj: " + scan.getAllAps().size());
//                            Fingerprint fp;
//                            dbHandler.getPosition(scan);
//                            if (dbHandler.getFplist().size() > 0) {
//                                fp = dbHandler.getFP(dbHandler.getFplist().get(0));
//                                Log.d("fp",
//                                        fp.getMapid() + "," + fp.getXcoord() + ","
//                                                + fp.getYcoord());
//
//
//                                Float[] res = new Float[2];
//                                res[0] = fp.getXcoord();
//                                res[1] = fp.getYcoord();
//                //                return res;
//
//                                canvasView.setLocalizedFHDMapsFingerprint(res);
//
//
//                //                int level = MapFragment.searchFloor(fp.getMapid()).getLevel();
//                //                mapFragment.levelListener.setColors(level);
//                //                if (findme) {
//                //                    mView.setMarker(fp.getXcoord(), fp.getYcoord(), level, 1);
//                //                    findme = false;
//                //                } else
//                //                    mView.moveMarker(fp.getXcoord(), fp.getYcoord(), level, 1);
//
//
//                        } else {
//                                String message = "Leider konnte Ihre Position nicht ermittelt werden";
//                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                                canvasView.setLocalizedFHDMapsFingerprint(null);
//                            }
//
//
//                            this.restartScan();
//                        }
//
//
//                //            wifiManager.startScan();
//
//                    }

}
