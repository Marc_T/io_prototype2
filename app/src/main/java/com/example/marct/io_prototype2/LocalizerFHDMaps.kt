package com.example.marct.io_prototype2

import android.content.Context
import android.content.Context.SENSOR_SERVICE
import com.example.marct.io_prototype2.compass.CompassListener
import android.support.v4.content.ContextCompat.getSystemService
import android.hardware.SensorManager
import android.hardware.SensorEvent
import android.hardware.Sensor.TYPE_ORIENTATION
import android.hardware.SensorEventListener
import android.R.string.cancel
import android.app.AlertDialog
import android.app.AppComponentFactory
import android.content.DialogInterface
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.content.Intent
import android.net.wifi.ScanResult
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.marct.io_prototype2.wifiData.Fingerprint


class LocalizerFHDMaps(context: Context, mainActivity: AppCompatActivity) {

    private lateinit var _context: Context

    private lateinit var mainactivity: AppCompatActivity

    var fingerprints: List<Fingerprint>? = null

    init {
        _context = context
        mainactivity = mainActivity
    }

    fun setFps(fps: List<Fingerprint>) {
        fingerprints = fps
    }

    fun findMe(javalocalizer: LocalizerFHDMapsJAVA,  newWifiList: List<ScanResult>, orientation: String): Array<Float>? {
        val fp = javalocalizer.findMe(newWifiList, orientation)
        return fp
    }

//    fun automaticFindMe(javalocalizer: LocalizerFHDMapsJAVA, canvasView: CanvasView, orientation: String) {
//
////        javalocalizer.startAutomaticFindMe(canvasView, orientation);
//    }

    fun makeFingerprint(mapPosition: MapPosition) {
        // Um diesem Problem
        // zu begegnen, werden für jeden Fingerprint Messwerte für alle vier Himmelsrichtungen
        // erstellt. Die Himmelsrichtung wird mittels des Kompasses im mobilen Endgerät ausgelesen.

        // himmelsrichtung herausfinden
//
//        val sm = _context.getSystemService(SENSOR_SERVICE) as SensorManager
//        val cl = CompassListener(sm)

        // NOTE: public void newFingerPrint(float drawThisX, float drawThisY) { aus FhdMaps




        val finalDrawX: Float = mapPosition.x
        val finalDrawY: Float = mapPosition.y

//        final float finalDrawY = drawThisY;

        val alertDialogBuilder = AlertDialog.Builder(_context)

        // set title
        alertDialogBuilder.setTitle("Neuer Messpunkt")

        // set dialog message
        alertDialogBuilder
                .setMessage("M�chten Sie einen neuen Messpunkt erstellen?")
                .setCancelable(false)
//                .setPositiveButton("Ja",
//                        DialogInterface.OnClickListener { dialog, id ->
//                            val fPrint = Fingerprint()
//                            fPrint.setXcoord(finalDrawX)
//                            fPrint.setYcoord(finalDrawY)
//                            fPrint.setMapid(getMapId(finalDrawX,
//                                    finalDrawY))
//
//                            val intent = Intent(getContext(),
//                                    ScanActivity::class.java)
//                            intent.putExtra("Fingerprint", fPrint)
//
//                            mf.startActivityForResult(intent, 1)
//                        })
                .setPositiveButton("Ja") {dialog, which ->
                    val fPrint = Fingerprint()
                    fPrint.xcoord = finalDrawX
                    fPrint.ycoord = finalDrawY
                    fPrint.mapid = 1
                    val intent = Intent(_context, ScanActivity::class.java)
                    intent.putExtra("Fingerprint", fPrint)
//                    startActivityForResult(intent, 1)

//                    startActivityForResult(ScanActivity(), intent, 1m)
//                    startActivityForResult(intent, 1)
                        mainactivity.startActivityForResult(intent, 1)

                }
                .setNegativeButton("Abbrechen"){dialog, which -> dialog.cancel()}


        // create alert dialog
        val alertDialog = alertDialogBuilder.create()

        // show it
        alertDialog.show()


    }

}

data class MapPosition(val x: Float, val y:Float)
