package com.example.marct.io_prototype2.wlanaccess.model;

public class AccessPoint {
    private String ssid = "";
    private String macAdress = "";
    private int frequency;
    private int sum,averageSignal, count;

    public int getFrequenzy() {
        return frequency;
    }

    public void setFrequenzy(int frequenzy) {
        this.frequency = frequenzy;
    }

    public AccessPoint(){

    }

    public AccessPoint(int signalStrength, String ssid, String macAdress, int frequenzy) {
        this.ssid = ssid;
        this.macAdress = macAdress;
        this.frequency = frequenzy;

        addSignal(signalStrength);
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }

    public void addSignal(int signal) {
        count++;
        sum += signal;
        this.averageSignal = sum / count;
    }

    public int getAverageSignal() {

        return averageSignal;
    }

    @Override
    public String toString() {
        return macAdress;
    }

}