package com.example.marct.io_prototype2

import android.content.pm.PackageManager
import android.os.Build


import android.Manifest
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Toast
import com.example.marct.io_prototype2.database.AppDataBase
import com.example.marct.io_prototype2.database.DbWorkerThread
import com.example.marct.io_prototype2.database.Point
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.content.ContextCompat.getSystemService
import android.net.wifi.WifiManager
import android.R.attr.level
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.content.ContextCompat.checkSelfPermission


class LocalizerA(context: Context) {

    private val LEVELS: Int = 999999999
    private lateinit var mWifiListener: WifiManager
    private lateinit var _context: Context
    public var fingerprints: MutableList<FingerprintA> = mutableListOf<FingerprintA>()
    private var cFingerprintId: Int = 0

    init {
        mWifiListener = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        _context = context
    }

    fun initialize()
    {
        settempFingerprint1()
        settempFingerprint2()

    }

    fun simplify() {
        val cellsize = 300
//        var cellTopX = 0
//        var cellTopY = 0
//        var cellBottomX = cellTopX + cellsize
//        var cellBottomY = cellTopY + cellsize

        val MAXX = 2600
        val MAXY = 2000

        var newFingerprints = mutableListOf<FingerprintA>()

        // for every cell
        for(x in 0..MAXX step cellsize) {
            for(y in 0..MAXY step cellsize) {
                var cellTopX = x.toFloat()
                var cellTopY = y.toFloat()
                var cellBottomX = (cellTopX + cellsize).toFloat()
                var cellBottomY = (cellTopY + cellsize).toFloat()

                // get every fingerprint in this cell
                var cFingerprints = mutableListOf<FingerprintA>()
                for(fingerprint in fingerprints) {
                    if(fingerprint.canvasPosX >= cellTopX && fingerprint.canvasPosY >= cellTopY
                        && fingerprint.canvasPosX < cellBottomX && fingerprint.canvasPosY < cellBottomY) {
                        cFingerprints.add(fingerprint)
                    }
                }

//                Log.d("FT1", "found ${cFingerprints.size} prints in cell X:${cellTopX} - ${cellBottomX} | Y:${cellTopY} - ${cellBottomY}")

                // for all found fingerprints in this cell, make 1 new fingerprint instead
                // that has middled values for each BSSID
                var newRouterinfos = mutableListOf<RouterInfoA>()
                for(fingerprint in cFingerprints) {
                    for(routerinfo in fingerprint.RouterInfos) {

                        var exists = false
                        var cLevel = 0

                        // check if info for this bssid exists already
                        for(newRouterinfo in newRouterinfos) {
                            if(newRouterinfo.BSSID == routerinfo.BSSID) {
                                exists = true
                                cLevel = newRouterinfo.level
                            }
                        }

                        //
                        if(exists) {
                            // middle value
//                            Log.d("FT1", "---")
//                            Log.d("FT1", "Last value: " + routerinfo.level)
//                            Log.d("FT1", "New value: " + cLevel)


                            var smallerValue = Math.min(routerinfo.level, cLevel)
                            var diff = Math.abs(routerinfo.level - cLevel)
                            var newLevel = smallerValue - (diff/2).toInt()
                            Log.d("FT1", "smallerValue: " + smallerValue)
                            Log.d("FT1", "diff: " + diff)
                            Log.d("FT1", "new: " + newLevel)
                            routerinfo.level = newLevel

//                            Log.d("FT1", "Updated value: " + routerinfo.level)


                        }
                        else {
                            // just add
                            newRouterinfos.add(RouterInfoA(routerinfo.BSSID, routerinfo.level))
                        }


                    }
                }


                if(cFingerprints.size > 0 ) {
                    // write new output string
                    var output = ""
                    for (cnewrouterinfo in newRouterinfos) {
                        output += cnewrouterinfo.BSSID + ": " + cnewrouterinfo.level + ", "
                    }

                    // create new fingerprint
                    cFingerprintId++

                    val newPosX = (cellTopX + (cellsize / 2)).toFloat()
                    val newPosY = (cellTopY + (cellsize / 2)).toFloat()

                    Log.d("FT1", "Updated 1 FP in cell X:${cellTopX} - ${cellBottomX} | Y:${cellTopY} - ${cellBottomY}")
                    var newfingerprint = FingerprintA(cFingerprintId, newPosX, newPosY, newRouterinfos, output)
                    newFingerprints.add(newfingerprint)

                }

            }

        }


        fingerprints = newFingerprints


    }

    fun makeFingerprint(canvasPosX: Float, canvasPosY: Float) {
//        Log.d("FT2", "1 " + canvasPosX + " " + canvasPosY)
        var routerinfos = mutableListOf<RouterInfoA>()

        var disregard = false

        // Level of a Scan Result
        val wifiList = mWifiListener.scanResults
        var output = ""
        var savedOutput = ""
        for (scanResult in wifiList) {
//            val level = WifiManager.calculateSignalLevel(scanResult.level, LEVELS)
            val level = scanResult.level


//            scanResult.

            output += scanResult.BSSID + ": " + level + ", "
            savedOutput += scanResult.BSSID + ": " + level + ", \n"


            if(level == (LEVELS - 1)) {
                disregard = true
                break
            }


            // durch alle bestehenden fingerprints gehen, wenn woanders die selbe bssid schon denselben level wert hat disregarden
            for(fingerprint in fingerprints) {
                for (wert in fingerprint.RouterInfos) {
                    if(wert.BSSID == scanResult.BSSID && wert.level == level) {
                        disregard = true
                        break
                    }
                }
            }





            val newRouterInfoA = RouterInfoA(scanResult.BSSID, level)
//            Log.d("FT1", "ADDED")
//            Log.d("FT1", newRouterInfoA.toString())
            routerinfos.add(newRouterInfoA)
        }
        Log.d("FT1", "ID: ${cFingerprintId} : " + output)


//        if(!disregard) {
            // add to fingerprints
            val newFingerprint = FingerprintA(cFingerprintId, canvasPosX, canvasPosY, routerinfos, savedOutput)
            fingerprints.add(newFingerprint)
            cFingerprintId++

//        Log.d("FT1", "ADDED")
            Toast.makeText(_context, "ADDED", Toast.LENGTH_SHORT).show()
//        }
//        else {
//            Toast.makeText(_context, "DISREGARDED", Toast.LENGTH_SHORT).show()
//        }
    }


    fun settempFingerprint1() {
        var routerinfos = mutableListOf<RouterInfoA>()

        // Level of a Scan Result
        val wifiList = mWifiListener.scanResults
        var output = ""
        var savedOutput = ""

            var newRouterInfoA = RouterInfoA("EasyBox-379456", 96)
            routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("MaBaKrMaNi", 46)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("EasyBox-838059", 50)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("EasyBox-52A840", 37)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("WLAN-33P679", 30)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("EasyBox-379456", 39)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("WLAN-490134", 24)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("MaBaKrMaNi", 17)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("rgr-wlan", 19)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("FRITZ!WLAN Repeater 310", 15)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("fightnet", 24)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("DIRECT-Zv-BRAVIA", 46)
        routerinfos.add(newRouterInfoA)


        output = "ID: 0 : EasyBox-379456: 96, MaBaKrMaNi: 46, EasyBox-838059: 50, EasyBox-52A840: 37, WLAN-33P679: 30, EasyBox-379456: 39, WLAN-490134: 24, MaBaKrMaNi: 17, rgr-wlan: 19, FRITZ!WLAN Repeater 310: 15, fightnet: 24, DIRECT-Zv-BRAVIA: 46, "

        savedOutput = output
        Log.d("FT1", "ID: ${cFingerprintId} : " + output)

        // add to fingerprints
        val newFingerprint = FingerprintA(cFingerprintId, 334.1906f, 1526.729f, routerinfos, savedOutput)
        fingerprints.add(newFingerprint)
        cFingerprintId++
    }


    fun settempFingerprint2() {
        var routerinfos = mutableListOf<RouterInfoA>()

        // Level of a Scan Result
        val wifiList = mWifiListener.scanResults
        var output = ""
        var savedOutput = ""

        var newRouterInfoA = RouterInfoA("EasyBox-379456", 55)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("MaBaKrMaNi", 46)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("EasyBox-838059", 50)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("EasyBox-52A840", 37)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("WLAN-33P679", 30)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("EasyBox-379456", 39)
        routerinfos.add(newRouterInfoA)


        newRouterInfoA = RouterInfoA("WLAN-490134", 24)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("MaBaKrMaNi", 17)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("rgr-wlan", 19)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("FRITZ!WLAN Repeater 310", 15)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("fightnet", 24)
        routerinfos.add(newRouterInfoA)

        newRouterInfoA = RouterInfoA("DIRECT-Zv-BRAVIA", 46)
        routerinfos.add(newRouterInfoA)

output = "EasyBox-379456: 55, MaBaKrMaNi: 46, EasyBox-838059: 50, EasyBox-52A840: 37, WLAN-33P679: 30, EasyBox-379456: 39, WLAN-490134: 24, MaBaKrMaNi: 17, rgr-wlan: 19, FRITZ!WLAN Repeater 310: 15, fightnet: 24, DIRECT-Zv-BRAVIA: 46, "
        savedOutput = output
        Log.d("FT1", "ID: ${cFingerprintId} : " + output)

        // add to fingerprints
        val newFingerprint = FingerprintA(cFingerprintId, 257.45053f, 372.68555f, routerinfos, savedOutput)
        fingerprints.add(newFingerprint)
        cFingerprintId++
    }

//    fun findMe(): FingerprintA? {
    fun findMe(): List<FingerprintA> {
        val wifiList = mWifiListener.scanResults

        var disregard = false

//
        var output = ""
        for (scanResult in wifiList) {
//            val level = WifiManager.calculateSignalLevel(scanResult.level, LEVELS)

            val level = scanResult.level

            if(level == (LEVELS - 1)) disregard = true

            output += scanResult.BSSID + ": " + level + ", "
        }
        Log.d("FT1", "ON LOCATE")
        Log.d("FT1", output)


//        if(!disregard) {
            // List A = Fingerprints
            // List B = current Scan
            var comparisonResults = mutableListOf<FingerprintComparisonResult>()


            // for every scanned fingerprint
            for (fingerprint in fingerprints) {
//            Log.d("FT1", "FingerprintID: " + fingerprint.id)
                var newFingerPrintComparisonResult = FingerprintComparisonResult(fingerprint.id, 0)

                // for every routerinfo in this fingerprint
                for (routerinfo in fingerprint.RouterInfos) {
//                Log.d("FT1", "RouterSSID: " + routerinfo.SSID)

                    // for every entry in the current list
                    for (scanResult in wifiList) {
//                    Log.d("FT1", "newSSID: " + scanResult.SSID)

                        // for every SSID that occurs in both lists
                        if (routerinfo.BSSID == scanResult.BSSID) {
                            // calculate difference between the levels
//                            val cLevel = WifiManager.calculateSignalLevel(scanResult.level, LEVELS)
                            val cLevel = scanResult.level
                            val diff = Math.abs(cLevel - routerinfo.level)

//                        Log.d("FT1", "SSID " + routerinfo.SSID + " FOUND IN BOTH WITH DIFF " + diff )

                            newFingerPrintComparisonResult.difference = newFingerPrintComparisonResult.difference + diff


                        }
                    }
                }


                comparisonResults.add(newFingerPrintComparisonResult)
            }


//         find the fingerpritn from results where the calculated difference is the smallest
            var lastDiff = Integer.MAX_VALUE
            var locatedFingerprintId: Int? = null
            for (comparisonResult in comparisonResults) {
                Log.d("FT1", "" + comparisonResult.toString())
                if (comparisonResult.difference < lastDiff) {
                    lastDiff = comparisonResult.difference
                    locatedFingerprintId = comparisonResult.fingerprintId
                }
            }


            Log.d("FT1", "BEST FINGERPRINT HAS ID: " + locatedFingerprintId)

//            var result: FingerprintA? = null
//
//            if (locatedFingerprintId == null) {
//                Toast.makeText(_context, "UNDEFINED?", Toast.LENGTH_SHORT).show()
//            } else {
//                // find the fingerprint object
//                for (fingerprint in fingerprints) {
//                    if (fingerprint.id == locatedFingerprintId) {
//                        result = fingerprint
//                    }
//                }
//            }
//            return result

        // Alle zurückgeben die die gleiche mindiff haben
        val minDiff = lastDiff

        var ids = mutableListOf<Int>()
        for(comparisonResult in comparisonResults) {
            if(comparisonResult.difference == minDiff) {
                ids.add(comparisonResult.fingerprintId)
            }
        }



        var bestfingerprints = mutableListOf<FingerprintA>()
        for(fingerprint in fingerprints) {
            if(ids.contains(fingerprint.id)) {
                bestfingerprints.add(fingerprint)
            }
        }

        return bestfingerprints









//        }
//        else {
//            Toast.makeText(_context, "DISREGARDED", Toast.LENGTH_SHORT).show()
//            return null
//        }


//        return null

    }
}


data class RouterInfoA(val BSSID: String, var level: Int)
data class FingerprintA(val id: Int, val canvasPosX: Float, val canvasPosY: Float, val RouterInfos: List<RouterInfoA>, val output: String)
data class FingerprintComparisonResult(val fingerprintId: Int, var difference: Int)