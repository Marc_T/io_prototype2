package com.example.marct.io_prototype2.localizerB;

import com.example.marct.io_prototype2.MapPosition;


public class LocalizerBPositionResult {
    public MapPosition mapPosition;
    public int drawRadius;
    public int drawColor;
    public int errorRadius;
    public String algoName;
    public LocalizerBPositionResult(MapPosition _mapPosition, int _drawRadius, int _drawColor, int _errorRadius, String _algoName) {
        mapPosition = _mapPosition;
        drawRadius = _drawRadius;
        drawColor = _drawColor;
        errorRadius = _errorRadius;
        algoName = _algoName;
    }
}
