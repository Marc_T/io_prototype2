package com.example.marct.io_prototype2.wlanaccess.model;


import java.util.ArrayList;

import com.example.marct.io_prototype2.Utility;
import com.example.marct.io_prototype2.wifiData.Fingerprint;

/**
 * Dient als Transfer-Objekt f�r den Algorithmus und enth�lt alle notwendigen
 * Daten die f�r den Algorithmus ben�tigt werden
 *
 */
public class Scan {

    private String direction;
    private Fingerprint fPrint;
    private ArrayList<AccessPoint> allAps;
    public static final String NORTH = "North";
    public static final String EAST = "East";
    public static final String SOUTH = "South";
    public static final String WEST = "West";

    public Scan(String direction, ArrayList<AccessPoint> allAps, Fingerprint fPrint){
//                                                        direction = Utility.removeSpecialCharacters(direction);
        this.direction = direction;
        this.allAps = allAps;
        this.fPrint = fPrint;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
//                                                                    direction = Utility.removeSpecialCharacters(direction);
        this.direction = direction;
    }

    public ArrayList<AccessPoint> getAllAps() {
        return allAps;
    }

    public void setAllAps(ArrayList<AccessPoint> allAps) {
        this.allAps = allAps;
    }

    public Fingerprint getfPrint() {
        return fPrint;
    }

    public void setfPrint(Fingerprint fPrint) {
        this.fPrint = fPrint;
    }


}
