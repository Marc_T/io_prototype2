package com.example.marct.io_prototype2.database;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;


final class InterfaceAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {
    public JsonElement serialize(T object, Type interfaceType, JsonSerializationContext context) {
        final JsonObject wrapper = new JsonObject();
        wrapper.addProperty("type", object.getClass().getName());
        wrapper.add("data", context.serialize(object));
        return wrapper;
    }

    public T deserialize(JsonElement elem, Type interfaceType, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject wrapper = (JsonObject) elem;
        final JsonElement typeName = get(wrapper, "type");
        final JsonElement data = get(wrapper, "data");
        final Type actualType = typeForName(typeName);
        return context.deserialize(data, actualType);
    }

    private Type typeForName(final JsonElement typeElem) {
        try {
            return Class.forName(typeElem.getAsString());
        } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }

    private JsonElement get(final JsonObject wrapper, String memberName) {
        final JsonElement elem = wrapper.get(memberName);
        if (elem == null) throw new JsonParseException("no '" + memberName + "' member found in what was expected to be an interface wrapper");
        return elem;
    }
}

//public class InterfaceAdapter implements JsonSerializer, JsonDeserializer {
//
//    private static final String CLASSNAME = "CLASSNAME";
//    private static final String DATA = "DATA";
//
//    public Object deserialize(JsonElement jsonElement, Type type,
//                         JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
//
//        JsonObject jsonObject = jsonElement.getAsJsonObject();
//        JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
//        String className = prim.getAsString();
//        Class klass = getObjectClass(className);
//        return jsonDeserializationContext.deserialize(jsonObject.get(DATA), klass);
//    }
//
//    public JsonElement serialize(Object jsonElement, Type type, JsonSerializationContext jsonSerializationContext) {
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty(CLASSNAME, jsonElement.getClass().getName());
//        jsonObject.add(DATA, jsonSerializationContext.serialize(jsonElement));
//        return jsonObject;
//    }
//
//    /****** Helper method to get the className of the object to be deserialized *****/
//    public Class getObjectClass(String className) {
//        try {
//            return Class.forName(className);
//        } catch (ClassNotFoundException e) {
//            //e.printStackTrace();
//            throw new JsonParseException(e.getMessage());
//        }
//    }
//}

